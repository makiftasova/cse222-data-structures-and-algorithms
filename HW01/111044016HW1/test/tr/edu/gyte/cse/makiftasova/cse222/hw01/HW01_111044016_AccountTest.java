package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_AccountTest {

    public HW01_111044016_AccountTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addFunds method, of class HW01_111044016_Account.
     */
    @Test
    public void testAddFunds() {
        System.out.println("addFunds");
        Double fundsToAdd = 100.0;
        HW01_111044016_Account instance = new HW01_111044016_Account();
        instance.addFunds(fundsToAdd);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getBalance method, of class HW01_111044016_Account.
     */
    @Test
    public void testGetBalance() {
        System.out.println("getBalance");
        HW01_111044016_Account instance = new HW01_111044016_Account(100.0);
        Double expResult = 100.0;
        Double result = instance.getBalance();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAccountNumber method, of class HW01_111044016_Account.
     */
    @Test
    public void testGetAccountNumber() {
        System.out.println("getAccountNumber");
        HW01_111044016_Account instance = new HW01_111044016_Account();
        String expResult = instance.getAccountNumber();
        String result = instance.getAccountNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getOwner method, of class HW01_111044016_Account.
     */
    @Test
    public void testGetOwner() {
        System.out.println("getOwner");
        HW01_111044016_Account instance = new HW01_111044016_Account();
        HW01_111044016_Customer expResult = null;
        HW01_111044016_Customer result = instance.getOwner();
        assertEquals(expResult, result);
    }

    /**
     * Test of removeFunds method, of class HW01_111044016_Account.
     */
    @Test
    public void testRemoveFunds() {
        System.out.println("removeFunds");
        Double fundToRemove = 120.0;
        HW01_111044016_Account instance = new HW01_111044016_Account();
        instance.removeFunds(fundToRemove);
    }

    /**
     * Test of setBalance method, of class HW01_111044016_Account.
     */
    @Test
    public void testSetBalance() {
        System.out.println("setBalance");
        Double balance = 20.3;
        HW01_111044016_Account instance = new HW01_111044016_Account();
        instance.setBalance(balance);
    }
}
