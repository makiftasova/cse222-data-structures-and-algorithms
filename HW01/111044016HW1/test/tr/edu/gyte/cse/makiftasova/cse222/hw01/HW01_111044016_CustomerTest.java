package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_CustomerTest {

    public HW01_111044016_CustomerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAccount method, of class HW01_111044016_Customer.
     */
    @Test
    public void testGetAccount() {
        System.out.println("getAccount");
        HW01_111044016_Customer instance = new HW01_111044016_Customer();
        HW01_111044016_Account a = new HW01_111044016_Account(instance, 120.20);
        HW01_111044016_Account expResult = a;
        HW01_111044016_Account result = instance.getAccount();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCustomerId method, of class HW01_111044016_Customer.
     */
    @Test
    public void testGetCustomerId() {
        System.out.println("getCustomerId");
        HW01_111044016_Customer instance = new HW01_111044016_Customer();
        String expResult = instance.getCustomerId();
        String result = instance.getCustomerId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class HW01_111044016_Customer.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        HW01_111044016_Customer instance = new HW01_111044016_Customer();
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAccount method, of class HW01_111044016_Customer.
     */
    @Test
    public void testSetAccount() {
        System.out.println("setAccount");
        HW01_111044016_Customer instance = new HW01_111044016_Customer();
        HW01_111044016_Account account = new HW01_111044016_Account(instance, 230.256);
        instance.setAccount(account);
    }

    /**
     * Test of setName method, of class HW01_111044016_Customer.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "test";
        HW01_111044016_Customer instance = new HW01_111044016_Customer();
        instance.setName(name);
    }

    /**
     * Test of equals method, of class HW01_111044016_Customer.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Object();
        HW01_111044016_Customer instance = new HW01_111044016_Customer();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
