package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_UserTest {
    
    public HW01_111044016_UserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of name method, of class HW01_111044016_User.
     */
    @Test
    public void testName() {
        System.out.println("name");
        HW01_111044016_User instance = new HW01_111044016_User();
        String expResult = "";
        String result = instance.name();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPassword method, of class HW01_111044016_User.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String oldPassword = "";
        String newPassword = "";
        HW01_111044016_User instance = new HW01_111044016_User();
        boolean expResult = false;
        boolean result = instance.setPassword(oldPassword, newPassword);
        assertEquals(expResult, result);
    }

    /**
     * Test of changePermissions method, of class HW01_111044016_User.
     */
    @Test
    public void testChangePermissions() {
        System.out.println("changePermissions");
        boolean newPermissionLevel = false;
        HW01_111044016_User instance = new HW01_111044016_User();
        boolean expResult = true;
        boolean result = instance.changePermissions(newPermissionLevel);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPermiisions method, of class HW01_111044016_User.
     */
    @Test
    public void testGetPermiisions() {
        System.out.println("getPermiisions");
        HW01_111044016_User instance = new HW01_111044016_User();
        int expResult = 1;
        int result = instance.getPermiisions();
        assertEquals(expResult, result);
    }

    /**
     * Test of isRoot method, of class HW01_111044016_User.
     */
    @Test
    public void testIsRoot() {
        System.out.println("isRoot");
        HW01_111044016_User instance = new HW01_111044016_User();
        boolean expResult = false;
        boolean result = instance.isRoot();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserTpye method, of class HW01_111044016_User.
     */
    @Test
    public void testGetUserTpye() {
        System.out.println("getUserTpye");
        HW01_111044016_User instance = new HW01_111044016_User();
        int expResult = 1;
        int result = instance.getUserTpye();
        assertEquals(expResult, result);
    }

    /**
     * Test of checkPassword method, of class HW01_111044016_User.
     */
    @Test
    public void testCheckPassword() {
        System.out.println("checkPassword");
        String passwordToCheck = "";
        HW01_111044016_User instance = new HW01_111044016_User();
        boolean expResult = true;
        boolean result = instance.checkPassword(passwordToCheck);
        assertEquals(expResult, result);
    }
}
