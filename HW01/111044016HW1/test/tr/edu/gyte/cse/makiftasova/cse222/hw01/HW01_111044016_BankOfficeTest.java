package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_BankOfficeTest {
    
    public HW01_111044016_BankOfficeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addWorker method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testAddWorker() {
        System.out.println("addWorker");
        HW01_111044016_Worker worker = new HW01_111044016_Worker("toAddWorker");
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        instance.addWorker(worker);
    }

    /**
     * Test of addCustomer method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testAddCustomer() {
        System.out.println("addCustomer");
        HW01_111044016_Customer customer = new HW01_111044016_Customer("test");
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        instance.addCustomer(customer);
    }

    /**
     * Test of getCustomers method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testGetCustomers() {
        System.out.println("getCustomers");
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        HW01_111044016_Customers cs = new HW01_111044016_Customers();
        instance.setCustomers(cs);
        HW01_111044016_Customers expResult = cs;
        HW01_111044016_Customers result = instance.getCustomers();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkers method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testGetWorkers() {
        System.out.println("getWorkers");
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        HW01_111044016_Workers ws = new HW01_111044016_Workers();
        HW01_111044016_Workers expResult = ws;
        instance.setWorkers(ws);
        HW01_111044016_Workers result = instance.getWorkers();
        assertEquals(expResult, result);
    }

    /**
     * Test of removeWorker method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testRemoveWorker() {
        System.out.println("removeWorker");
        HW01_111044016_Worker worker = new HW01_111044016_Worker("test worker");
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        instance.addWorker(worker);
        HW01_111044016_Worker expResult = worker;
        HW01_111044016_Worker result = instance.removeWorker(worker);
        assertEquals(expResult, result);
    }

    /**
     * Test of setCustomers method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testSetCustomers() {
        System.out.println("setCustomers");
        HW01_111044016_Customers customers = new HW01_111044016_Customers();
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        instance.setCustomers(customers);
    }

    /**
     * Test of setName method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "new name";
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        instance.setName(name);
    }

    /**
     * Test of setWorkers method, of class HW01_111044016_BankOffice.
     */
    @Test
    public void testSetWorkers() {
        System.out.println("setWorkers");
        HW01_111044016_Workers workers = new HW01_111044016_Workers();
        HW01_111044016_BankOffice instance = new HW01_111044016_BankOffice();
        instance.setWorkers(workers);
    }
}
