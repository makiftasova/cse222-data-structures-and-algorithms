package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_WorkersTest {

    public HW01_111044016_WorkersTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class HW01_111044016_Workers.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        HW01_111044016_Worker worker = new HW01_111044016_Worker("test Worker");
        HW01_111044016_Workers instance = new HW01_111044016_Workers();
        instance.add(worker);
    }

    /**
     * Test of setOffice method, of class HW01_111044016_Workers.
     */
    @Test
    public void testSetOffice() {
        System.out.println("setOffice");
        HW01_111044016_BankOffice office = new HW01_111044016_BankOffice("Test Office");
        HW01_111044016_Workers instance = new HW01_111044016_Workers();
        instance.setOffice(office);
    }
}
