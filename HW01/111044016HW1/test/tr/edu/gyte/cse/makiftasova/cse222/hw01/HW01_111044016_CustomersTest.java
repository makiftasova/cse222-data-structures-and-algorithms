package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_CustomersTest {
    
    public HW01_111044016_CustomersTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class HW01_111044016_Customers.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        HW01_111044016_Customer customer = new HW01_111044016_Customer("test customer");
        HW01_111044016_Customers instance = new HW01_111044016_Customers();
        instance.add(customer);
    }

    /**
     * Test of remove method, of class HW01_111044016_Customers.
     */
    @Test
    public void testRemove_Customer() {
        System.out.println("remove");
        HW01_111044016_Customer customer = new HW01_111044016_Customer("Test Customer");
        HW01_111044016_Customers instance = new HW01_111044016_Customers();
        instance.add(customer);
        HW01_111044016_Customer expResult = customer;
        HW01_111044016_Customer result = instance.remove(customer);
        assertEquals(expResult, result);
    }
}
