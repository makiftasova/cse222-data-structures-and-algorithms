package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_WorkerTest {

    public HW01_111044016_WorkerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getOffice method, of class HW01_111044016_Worker.
     */
    @Test
    public void testGetOffice() {
        System.out.println("getOffice");
        HW01_111044016_Worker instance = new HW01_111044016_Worker();
        HW01_111044016_BankOffice o = new HW01_111044016_BankOffice("Test office");
        instance.setOffice(o);
        HW01_111044016_BankOffice expResult = o;
        HW01_111044016_BankOffice result = instance.getOffice();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOffice method, of class HW01_111044016_Worker.
     */
    @Test
    public void testSetOffice() {
        System.out.println("setOffice");
        HW01_111044016_BankOffice office = new HW01_111044016_BankOffice("Test office");
        HW01_111044016_Worker instance = new HW01_111044016_Worker();
        instance.setOffice(office);
    }
}
