package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_UsersTest {

    public HW01_111044016_UsersTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addUser method, of class HW01_111044016_Users.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        HW01_111044016_User user = new HW01_111044016_User("root", "toor", true);
        HW01_111044016_Users instance = new HW01_111044016_Users();
        instance.addUser(user);
    }

    /**
     * Test of addUser method, of class HW01_111044016_Users.
     */
    @Test
    public void testAddUser1() {
        System.out.println("addUser");
        HW01_111044016_User user = new HW01_111044016_User("worker", "rekrow", false);
        HW01_111044016_Users instance = new HW01_111044016_Users();
        instance.addUser(user);
    }

    /**
     * Test of removeUser method, of class HW01_111044016_Users.
     */
    @Test
    public void testRemoveUser() {
        System.out.println("removeUser");
        HW01_111044016_User user = new HW01_111044016_User("worker", "rekrow", false);
        HW01_111044016_Users instance = new HW01_111044016_Users();
        instance.addUser(user);
        HW01_111044016_User expResult = user;
        HW01_111044016_User result = instance.removeUser(user);
        assertEquals(expResult, result);
    }
}
