package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_BankOfficesTest {
    
    public HW01_111044016_BankOfficesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class HW01_111044016_BankOffices.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        HW01_111044016_BankOffice office = new HW01_111044016_BankOffice("Test Office");
        HW01_111044016_BankOffices instance = new HW01_111044016_BankOffices();
        instance.add(office);
    }

    /**
     * Test of findIndex method, of class HW01_111044016_BankOffices.
     */
    @Test
    public void testFindIndex() {
        System.out.println("findIndex");
        HW01_111044016_BankOffice office = new HW01_111044016_BankOffice("Testing");
        HW01_111044016_BankOffices instance = new HW01_111044016_BankOffices();
        instance.add(office);
        int expResult = 0;
        int result = instance.findIndex(office);
        assertEquals(expResult, result);
    }

    /**
     * Test of remove method, of class HW01_111044016_BankOffices.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        HW01_111044016_BankOffice office = new HW01_111044016_BankOffice("toRemove");
        HW01_111044016_BankOffices instance = new HW01_111044016_BankOffices();
        instance.add(office);
        HW01_111044016_BankOffice expResult = office;
        HW01_111044016_BankOffice result = instance.remove(office);
        assertEquals(expResult, result);
    }
}
