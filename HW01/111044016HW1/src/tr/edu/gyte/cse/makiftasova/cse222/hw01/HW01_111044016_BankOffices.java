package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_BankOffices {

    private ArrayList<HW01_111044016_BankOffice> ml_offices;
    private final String mfs_fileName = "111044016HW01offices.bin";
    HW01_111044016_BinaryInput mbi_binaryInput;
    HW01_111044016_BinaryOutput mbo_binaryOutput;

    /**
     * No parameter Constructor. Initializes Offices list with an empty
     * ArrayList
     */
    public HW01_111044016_BankOffices() {
        ml_offices = new ArrayList<>();
    }

    /**
     * Adds given office to list
     *
     * @param office new Office
     */
    public void add(HW01_111044016_BankOffice office) {
        ml_offices.add(office);
    }

    /**
     * Finds index of given office then returns it
     *
     * @param office Office to find index
     * @return Index of office
     */
    public int findIndex(HW01_111044016_BankOffice office) {
        return ml_offices.indexOf(office);
    }

    /**
     * Finds given office at list and returns it
     *
     * @param office Office to find
     * @return Found office
     */
    public HW01_111044016_BankOffice find(HW01_111044016_BankOffice office) {
        return ml_offices.get(ml_offices.indexOf(findIndex(office)));
    }

    /**
     * Removes given Office from list, then returns it
     *
     * @param office Office to remove
     * @return Removed Office
     */
    public HW01_111044016_BankOffice remove(HW01_111044016_BankOffice office) {
        return ml_offices.remove(findIndex(office));
    }

    /**
     * Loads HW01_111044016_BankOffices data from file
     */
    public void loadFromFile() {
        try {
            Object tmp = null;
            mbi_binaryInput = new HW01_111044016_BinaryInput(mfs_fileName);
            while (null != (tmp = mbi_binaryInput.readFromFile())) {
                ml_offices.add((HW01_111044016_BankOffice) tmp);
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace(System.err);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
    }

    /**
     * Saves Current HW01_111044016_BankOffices data to file
     */
    public void saveToFile() {
        try {
            mbo_binaryOutput = new HW01_111044016_BinaryOutput(mfs_fileName);
            for (HW01_111044016_BankOffice bo : ml_offices) {
                mbo_binaryOutput.writeToFileFrom(bo);
            }
            mbo_binaryOutput.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace(System.err);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
    }

    /**
     * Returns reference of Bank Offices ArrayList object
     *
     * @return Reference of Bank Offices ArrayList object
     */
    public ArrayList<HW01_111044016_BankOffice> toArrayList() {
        return ml_offices;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (HW01_111044016_BankOffice b : ml_offices) {
            s.append(b).append("\n");
        }
        return s.toString();
    }
}//end HW01_111044016_BankOffices