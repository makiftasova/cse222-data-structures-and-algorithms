package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.util.Objects;

/**
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_Customer implements HW01_111044016_Person {

    private HW01_111044016_Account ma_account;
    private String ms_id;
    private String ms_name;
    /**
     * Static int value for determining id of customers
     */
    private static Integer si_nextId = 1;

    /**
     * No parameter constructor. Assigns null to everything but HW01_111044016_Customer ID.
     */
    public HW01_111044016_Customer() {
        ms_name = "";
        ma_account = null;
        ms_id = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * One parameter constructor. Assigns null to account of HW01_111044016_Customer
     *
     * @param name Name of HW01_111044016_Customer to create
     */
    @Deprecated
    public HW01_111044016_Customer(String name) {
        ms_name = name;
        ma_account = null;
        ms_id = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * Two parameter constructor
     *
     * @param account HW01_111044016_Account of HW01_111044016_Customer to create
     * @param name Name of HW01_111044016_Customer to create
     */
    public HW01_111044016_Customer(String name, HW01_111044016_Account account) {
        ms_name = name;
        ma_account = account;
        ma_account.setOwner(this);
        ms_id = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * Gives HW01_111044016_Account of customer
     *
     * @return HW01_111044016_Account of customer
     */
    @Override
    public HW01_111044016_Account getAccount() {
        return ma_account;
    }

    /**
     * Gives HW01_111044016_Customer ID
     *
     * @return customerID of customer
     */
    public String getCustomerId() {
        return ms_id;
    }

    /**
     * Gives name of customer
     *
     * @return Name of HW01_111044016_Customer
     */
    @Override
    public String getName() {
        return ms_name;
    }

    /**
     * Changes HW01_111044016_Account of HW01_111044016_Customer to given one
     *
     * @param account new HW01_111044016_Account of HW01_111044016_Customer
     */
    @Override
    public void setAccount(HW01_111044016_Account account) {
        ma_account = account;
        ma_account.setOwner(this);
    }

    /**
     * Changes name of HW01_111044016_Customer with given one
     *
     * @param name new Name of HW01_111044016_Customer
     */
    @Override
    public void setName(String name) {
        ms_name = name;
    }

    /**
     * A method for comparing two instances of HW01_111044016_Customer Class
     *
     * @param customer HW01_111044016_Customer to compare
     * @return true if both objects HW01_111044016_Customer ID values are same
     */
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof HW01_111044016_Customer) {
            result = (this.hashCode() == obj.hashCode());
        }
        return result;
    }

    /**
     * HashCode generator for HW01_111044016_Customer objects.
     *
     * @return HashCode of object
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.ma_account);
        hash = 11 * hash + Objects.hashCode(this.ms_id);
        hash = 11 * hash + Objects.hashCode(this.ms_name);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Name: ");
        s.append(ms_name).append(" Id:").append(ms_id).append(" Balance: ");
        s.append(ma_account.getBalance().toString());
        return s.toString();

    }
}//end HW01_111044016_Customer