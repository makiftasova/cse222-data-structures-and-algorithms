package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_Customers implements Serializable {

    private ArrayList<HW01_111044016_Customer> ml_customers;
    private HW01_111044016_BankOffice mb_BankOffice;

    /**
     * No parameter Constructor. Assigns null to Bank Office
     */
    public HW01_111044016_Customers() {
        ml_customers = new ArrayList<>();
        mb_BankOffice = null;
    }

    /**
     * One Parameter Constructor. Take office of customer. Initialize HW01_111044016_Customer
     * list as an empty ArrayList. Also changes Given office's customer list
     * with itself
     *
     * @param ofice Office of HW01_111044016_Customers
     */
    public HW01_111044016_Customers(HW01_111044016_BankOffice ofice) {
        ml_customers = new ArrayList<>();
        mb_BankOffice = ofice;
        mb_BankOffice.setCustomers(this);
    }

    /**
     * Adds a customer to list
     *
     * @param customer new customer to add
     */
    public void add(HW01_111044016_Customer customer) {
        ml_customers.add(customer);
    }

    /**
     * Sets Office of HW01_111044016_Customer
     *
     * @param office New office of customer
     */
    public void setOffice(HW01_111044016_BankOffice office) {
        mb_BankOffice = office;
    }

    /**
     * Returns office of HW01_111044016_Customers
     *
     * @return office of HW01_111044016_Customers
     */
    public HW01_111044016_BankOffice getOffice() {
        return mb_BankOffice;
    }

    /**
     * Finds index of given customer
     *
     * @param customer Index of customer
     */
    public int findIndex(HW01_111044016_Customer customer) {
        int index = -1;
        int i = 0;
        for (HW01_111044016_Customer c : ml_customers) {
            if (customer.equals(c)) {
                index = i;
            }
            ++i;
        }
        return index;
    }

    /**
     * Removes HW01_111044016_Customer at given index
     *
     * @param index Index of customer to remove
     * @return Removed HW01_111044016_Customer
     */
    public HW01_111044016_Customer remove(int index) {
        return ml_customers.remove(index);
    }

    /**
     * Removes given customer from HW01_111044016_Customers list. and returns it
     *
     * @param customer HW01_111044016_Customer to remove
     * @return Removed customer
     */
    public HW01_111044016_Customer remove(HW01_111044016_Customer customer) {
        return this.remove(this.findIndex(customer));
    }

    /**
     * Returns a reference to ArrayList which contains HW01_111044016_Customers.
     *
     * @return All customers in an ArrayList
     */
    public ArrayList<HW01_111044016_Customer> toArrayList() {
        return ml_customers;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (HW01_111044016_Customer c : ml_customers) {
            s.append(c).append("\n");
        }
        return s.toString();
    }
}//end HW01_111044016_Customers