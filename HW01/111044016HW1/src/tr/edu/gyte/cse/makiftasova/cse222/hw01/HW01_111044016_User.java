package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.Serializable;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_User extends Object implements Serializable {

    private String ms_name;
    private String ms_password;
    /**
     * Permissions flag for user. true means System Administrator and false
     * means Worker
     */
    private boolean mb_isRoot;
    /**
     * HW01_111044016_User ID of root user
     */
    public static final int USER_ROOT = 0;
    /**
     * HW01_111044016_User ID of worker user
     */
    public static final int USER_WORKER = 1;
    /**
     * HW01_111044016_User ID of common user (e.g. Customer)
     */
    public static final int USER_NORMAL = 2;

    /**
     * Generates a new HW01_111044016_User. assigns empty String to password and username
     * fields. Also Grants Worker permission to HW01_111044016_User
     */
    public HW01_111044016_User() {
        ms_name = "";
        ms_password = "";
        mb_isRoot = false;
    }

    /**
     * Generates new user with given username. Assigns username to Password
     * field. Also Grants Worker permission to HW01_111044016_User
     *
     * @param name Username of HW01_111044016_User
     */
    public HW01_111044016_User(String name) {
        ms_name = name;
        ms_password = new String(ms_name);
        mb_isRoot = false;
    }

    /**
     * Generates a New HW01_111044016_User with given username and password. Also Grants Worker
     * permission to HW01_111044016_User
     *
     * @param name Username of HW01_111044016_User
     * @param password Password of HW01_111044016_User
     */
    public HW01_111044016_User(String name, String password) {
        ms_name = name;
        ms_password = password;
        mb_isRoot = false;
    }

    /**
     * Generates a New HW01_111044016_User with given username, password and permissions.
     *
     * @param name Username of HW01_111044016_User
     * @param password Password of HW01_111044016_User
     * @param permissions Permission level of user
     */
    public HW01_111044016_User(String name, String password, boolean isRoot) {
        ms_name = name;
        ms_password = password;
        mb_isRoot = isRoot;
    }

    /**
     * Returns username of user
     *
     * @return Username of user
     */
    public String name() {
        return ms_name;
    }

    /**
     * Changes user password with given new password
     *
     * @param oldPassword Password of user to change. required for security
     * @param newPassword Desired Password of user
     * @return If password changed true, else false
     */
    public boolean setPassword(String oldPassword, String newPassword) {
        boolean status = false;

        if ((!ms_password.equals(newPassword))
                && (ms_password.equals(oldPassword))) {
            ms_password = newPassword;
            status = true;
        }

        return status;
    }

    /**
     * Changes users permission with given one. This is very dangerous method,
     * if it falls to wrong hands. So only root should have access to this
     * method. If someone changes to root's permissions, method will return
     * false, and does not complete operation
     *
     * @param newPermissionLevel new Permission level of user
     * @return If permissions of root users is changing returns false, else true
     */
    public boolean changePermissions(boolean newPermissionLevel) {
        boolean status = false;
        if (!mb_isRoot) {
            mb_isRoot = newPermissionLevel;
            status = true;
        }

        return status;
    }

    /**
     * Returns Permission level of user
     *
     * @return Permission level of user
     */
    public int getPermiisions() {
        return (mb_isRoot ? USER_ROOT : USER_WORKER);
    }

    /**
     * If user is root returns true, else false
     *
     * @return If user is root returns true, else false
     */
    public boolean isRoot() {
        return mb_isRoot;
    }

    /**
     * Returns type of HW01_111044016_User. If user is root returns USER_ROOT and if user is
     * worker returns USER_WORKER
     *
     * @return Type of HW01_111044016_User.
     */
    public int getUserTpye() {
        return (mb_isRoot ? USER_ROOT : USER_WORKER);
    }

    /**
     * Returns True if given password if correct else false
     *
     * @return True if given password if correct else false
     */
    public boolean checkPassword(String passwordToCheck) {
        return (ms_password.equals(passwordToCheck));
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(mb_isRoot ? "ROOT" : "WORKER").append(" | username: ");
        s.append(ms_name).append(" | password: ").append(ms_password);
        return s.toString();
    }
}
