package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public final class HW01_111044016_BinaryOutput extends Object {

    private String ms_fileName;
    private FileOutputStream mfos_outputFile;
    private ObjectOutputStream moos_binOut;
    /**
     * Default output file name for class
     */
    public static final String DEFAULT_FILE_NAME = "Data.bin";

    /**
     * Default constructor. Created a file which has same name with
     * DEFAULT_FILE_NAME
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public HW01_111044016_BinaryOutput() throws FileNotFoundException, IOException {
        this.setFileName(HW01_111044016_BinaryOutput.DEFAULT_FILE_NAME);

        this.mfos_outputFile = new FileOutputStream(ms_fileName);
        this.moos_binOut = new ObjectOutputStream(mfos_outputFile);

    }

    /**
     * Creates a HW01_111044016_BinaryOutput object which works on file which has given name
     *
     * @param fileName Name of file to manipulate. Must contain extension of
     * file
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public HW01_111044016_BinaryOutput(String fileName) throws FileNotFoundException,
            IOException {

        this.setFileName(fileName);

        this.mfos_outputFile = new FileOutputStream(ms_fileName);
        this.moos_binOut = new ObjectOutputStream(mfos_outputFile);
    }

    /**
     * Declared for closing file and object streams at object's death
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        moos_binOut.close();
        mfos_outputFile.close();
    }

    /**
     * Changes the currently manipulating file
     *
     * @param fileName Name of file to manipulate. Must contain extension of
     * file
     */
    public void setFileName(String fileName) {
        this.ms_fileName = fileName;
    }

    /**
     * Returns current file name of HW01_111044016_BinaryOutput object
     *
     * @return Current file name of HW01_111044016_BinaryOutput object
     */
    public String name() {
        return ms_fileName;
    }

    /**
     * Closes Binary I/O streams whose are included in HW01_111044016_BinaryOutput class
     *
     * @return true if stream are closed successfully, false if there any error
     */
    public boolean close() {
        boolean returnValue = true;

        try {
            if (null != moos_binOut) {
                moos_binOut.close();
            }

            if (null != mfos_outputFile) {
                mfos_outputFile.close();
            }
        } catch (IOException e) {
            // TODO change output buffer with a log buffer(or file)
            e.printStackTrace(System.err);
            returnValue = false;
        }

        return returnValue;

    }

    /**
     * Writes given BalanceEntry object to binary file.
     *
     * @param entry BalanceEntry object
     *
     * @return true if success
     */
    public boolean writeToFileFrom(Object entry) throws IOException {
        moos_binOut.writeObject(entry);

        return true;
    }
} // end HW01_111044016_BinaryOutput