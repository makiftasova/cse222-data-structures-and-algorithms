package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.util.Scanner;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String uname, upwd;
        boolean isLogedIn = false, logonDone = false;
        String choice;
        HW01_111044016_User activeUser = null;
        int count = 0;


        HW01_111044016_Users userList = new HW01_111044016_Users();
        userList.loadFromFile();

        System.out.println("[TEST RUN] PRINTING AVAIBLE USER'S DATA...");
        System.out.println(userList);
        System.out.println("[TEST RUN] EN OF USER DATA\n\n");


        HW01_111044016_UserInterface.printMainMenu();
        choice = scan.nextLine();

        if (choice.startsWith("1")) {
            isLogedIn = true;
        }

        if (isLogedIn) {



            while ((!logonDone) && (3 > count)) {

                System.out.print("Enter User Name:");
                uname = scan.nextLine();

                for (HW01_111044016_User u : userList.toArrayList()) {
                    if (u.name().equals(uname)) {
                        System.out.print("Enter password:");
                        upwd = scan.nextLine();
                        logonDone = u.checkPassword(upwd);
                        if (logonDone) {
                            activeUser = u;
                            break;
                        }
                    }
                }

                if (!logonDone) {
                    ++count;
                    System.out.println("Error!!!");
                }

            }

            try {
                System.out.print("Active User: " + activeUser.toString() + "\n");
            } catch (RuntimeException ex) {
                System.err.println("Runtime Exception at User Login!!!");
                System.exit(-2);
            }

        }


        int permissions = 2;

        if (null == activeUser) {
            permissions = 2;
        } else if (activeUser.isRoot()) {
            permissions = 0;
        } else {
            permissions = 1;
        }



        System.out.println("\n\n\n");


        boolean exit = false;

        while (true) {
            if (exit) {
                System.out.println("Now Exiting...");
                System.exit(0);
            }
            HW01_111044016_UserInterface.printOperationsMenu(permissions);

            choice = scan.nextLine().toUpperCase();

            HW01_111044016_BankOffices offices = new HW01_111044016_BankOffices();

            switch (choice) {
                case "X":
                    exit = true;
                    break;
                case "1": // Office operations
                    if (0 == permissions) { // if user is root
                    }
                    System.out.println("UNDER DEVELOPMENT!");
                    break;
                case "2": // HW01_111044016_User operations
                    if (0 == permissions) { // if user is root
                    }
                    System.out.println("UNDER DEVELOPMENT!");
                    break;
                case "3": // Customer operations
                    if (2 > permissions) { // if user is root or worker
                    }
                    System.out.println("UNDER DEVELOPMENT!");
                    break;
                case "4": // Worker operations
                    if (2 > permissions) { // if user is root or worker
                    }
                    System.out.println("UNDER DEVELOPMENT!");
                    break;
                case "5": // add funds to account
                    // anyone can come here
                    System.out.println("UNDER DEVELOPMENT!");
                    break;
                case "6": // remove funds to account
                    // anyone can come here
                    System.out.println("UNDER DEVELOPMENT!");
                    break;
                default:
                    System.out.println("No Such Choice " + choice);
                    break;
            }

        }









    }
}
