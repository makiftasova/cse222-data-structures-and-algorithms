package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_Workers implements Serializable {

    private ArrayList<HW01_111044016_Worker> ml_workers;
    private HW01_111044016_BankOffice mb_BankOffice;

    /**
     * No Parameter Constructor. Assigns null to Bank Office
     */
    public HW01_111044016_Workers() {
        ml_workers = new ArrayList<>();
        mb_BankOffice = null;

    }

    /**
     * One Parameter Constructor. Assigns given Bank Office to its Bank Office.
     * Also changes HW01_111044016_Workers of Bank Office to itself
     *
     * @param bankOffice Office of HW01_111044016_Workers
     */
    public HW01_111044016_Workers(HW01_111044016_BankOffice bankOffice) {
        ml_workers = new ArrayList<>();
        mb_BankOffice = bankOffice;
        mb_BankOffice.setWorkers(this);

    }

    /**
     * Adds a new HW01_111044016_Worker to list
     *
     * @param worker new HW01_111044016_Worker
     */
    public void add(HW01_111044016_Worker worker) {
        ml_workers.add(worker);
    }

    /**
     * Changes office of workers
     *
     * @param office New office of workers
     */
    public void setOffice(HW01_111044016_BankOffice office) {
        mb_BankOffice = office;
    }

    /**
     * Finds given worker's index
     *
     * @param worker HW01_111044016_Worker to find index
     */
    public int findIndex(HW01_111044016_Worker worker) {
        int index = -1;
        int i = 0;
        for (HW01_111044016_Worker w : ml_workers) {
            if (worker.equals(w)) {
                index = i;
            }
            ++i;
        }
        return index;
    }

    /**
     * Removes HW01_111044016_Worker at given index, then returns it.
     */
    public HW01_111044016_Worker remove(int index) {
        return ml_workers.remove(index);
    }

    /**
     * Removes given worker from list, then returns it.
     *
     * @param worker HW01_111044016_Worker to remove from list
     */
    public HW01_111044016_Worker remove(HW01_111044016_Worker worker) {
        return this.remove(this.findIndex(worker));
    }

    /**
     * Returns reference of HW01_111044016_Workers List as ArrayList
     *
     * @return HW01_111044016_Workers List as ArrayList
     */
    public ArrayList<HW01_111044016_Worker> toArrayList() {
        return ml_workers;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (HW01_111044016_Worker w : ml_workers) {
            s.append(w).append("\n");
        }

        return s.toString();
    }
}//end HW01_111044016_Workers