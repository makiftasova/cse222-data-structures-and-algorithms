package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public final class HW01_111044016_BinaryInput extends Object {

    private String ms_fileName;
    private FileInputStream mfis_inputFile;
    private ObjectInputStream mois_binIn;
    /**
     * Default input file name for class
     */
    public static final String DEFAULT_FILE_NAME = "Data.bin";

    /**
     * Default Constructor for class. Tries to read data from file which has
     * same name with DEFAULT_FILE_NAME
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public HW01_111044016_BinaryInput() throws FileNotFoundException, IOException {
        // TODO Auto-generated constructor stub

        this.setFileName(HW01_111044016_BinaryInput.DEFAULT_FILE_NAME);

        this.mfis_inputFile = new FileInputStream(ms_fileName);
        this.mois_binIn = new ObjectInputStream(mfis_inputFile);
    }

    /**
     * One parameter Constructor for class. Tries to read data from file which
     * has same name with fileName
     *
     * @param fileName Name of file to read data
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public HW01_111044016_BinaryInput(String fileName) throws FileNotFoundException,
            IOException {

        this.setFileName(fileName);
        this.mfis_inputFile = new FileInputStream(ms_fileName);
        this.mois_binIn = new ObjectInputStream(mfis_inputFile);

    }

    /**
     * Declared for closing file and object streams at object's death
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mois_binIn.close();
        mfis_inputFile.close();
    }

    /**
     * Changes the currently manipulating file
     *
     * @param fileName Name of file to manipulate. Must contain extension of
     * file
     */
    public void setFileName(String fileName) {
        this.ms_fileName = fileName;
    }

    /**
     * Returns current file name of BinaryOutput object
     *
     * @return Current file name of BinaryOutput object
     */
    public String name() {
        return ms_fileName;
    }

    /**
     * Closes Binary I/O streams whose are included in BinaryOutput class
     *
     * @return true if stream are closed successfully, false if there any error
     */
    public boolean close() {
        boolean returnValue = true;

        try {
            if (null != mois_binIn) {
                mois_binIn.close();
            }

            if (null != mfis_inputFile) {
                mfis_inputFile.close();
            }
        } catch (IOException e) {
            // TODO change output buffer with a log buffer(or file)
            e.printStackTrace(System.err);
            returnValue = false;
        }

        return returnValue;

    }

    /**
     * Reads a BalanceEntry object from binary file. returns null if no object
     * read.
     *
     * @return If success read object, else null
     */
    public Object readFromFile() {
        Object tmp = null;

        try {
            tmp = mois_binIn.readObject();
        } catch (ClassNotFoundException | FileNotFoundException ex) {
            // TODO change output buffer with a log buffer(or file)
            ex.printStackTrace(System.err);
        } catch (EOFException ex) {
            // TODO: handle exception
            tmp = null;
        } catch (IOException ex) {
            // TODO change output buffer with a log buffer(or file)
            ex.printStackTrace(System.err);
        }

        return tmp;
    }
} // end HW01_111044016_BinaryInput