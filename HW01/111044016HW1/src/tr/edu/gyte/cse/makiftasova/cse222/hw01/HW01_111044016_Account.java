package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.Serializable;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_Account implements Serializable {

    private HW01_111044016_Customer mc_customer;
    private Double md_balance;
    private String ms_accountNumber;
    private static Integer si_nextId = 1;

    /**
     * No Parameter COnstructor. Assigns null to owner of account. HW01_111044016_Account's
     * balance is 0 (zero)
     */
    public HW01_111044016_Account() {
        mc_customer = null;
        md_balance = 0.0;
        ms_accountNumber = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * Creates an account with given balance. Assigns null to owner of account.
     *
     * @param balance Balance of HW01_111044016_Account
     */
    public HW01_111044016_Account(Double balance) {
        this.mc_customer = null;
        this.md_balance = balance;
        ms_accountNumber = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * Created an account to given owner with given balance
     *
     * @param owner Owner of account
     * @param balance Balance of account
     */
    public HW01_111044016_Account(HW01_111044016_Customer owner, Double balance) {
        mc_customer = owner;
        mc_customer.setAccount(this);
        md_balance = balance;
        ms_accountNumber = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * Adds given amount of funds to account
     *
     * @param fundsToAdd Amount of funds to add to account balance
     */
    public void addFunds(Double fundsToAdd) {
        md_balance += fundsToAdd;
    }

    /**
     * Returns current balance of account
     *
     * @return Current balance of account
     */
    public Double getBalance() {
        return md_balance;
    }

    /**
     * Returns HW01_111044016_Account ID
     *
     * @return HW01_111044016_Account ID
     */
    public String getAccountNumber() {
        return ms_accountNumber;
    }

    /**
     * Returns Owner of account
     *
     * @return Owner of account
     */
    public HW01_111044016_Customer getOwner() {
        return mc_customer;
    }

    /**
     * Subtracts given amount of funds from account balance
     *
     * @param fundToRemove Amount of balance to subtract
     */
    public void removeFunds(Double fundToRemove) {
        md_balance -= fundToRemove;
    }

    /**
     * Changes HW01_111044016_Account Balance to given one
     *
     * @param balance new Balance of HW01_111044016_Account
     */
    @Deprecated
    public void setBalance(Double balance) {
        md_balance = balance;
    }

    /**
     * Changes Owner of account
     *
     * @param owner New Owner of account
     */
    public void setOwner(HW01_111044016_Customer owner) {
        mc_customer = owner;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Account ID: ");
        s.append(ms_accountNumber).append(" Owner: ");
        s.append(mc_customer.getName()).append(" Balance:").append(md_balance);
        return s.toString();
    }
}//end HW01_111044016_Account