package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.Serializable;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_BankOffice implements Serializable {

    private String ms_id;
    private HW01_111044016_Customers mc_customers;
    private HW01_111044016_Workers mw_workers;
    private String ms_name;
    private static Integer si_nextId = 1;

    /**
     * No Parameter Constructor. Creates empty HW01_111044016_Customers and HW01_111044016_Workers objects and
     * also assigns null to name of Office.
     */
    public HW01_111044016_BankOffice() {
        mc_customers = new HW01_111044016_Customers();
        mc_customers.setOffice(this);
        mw_workers = new HW01_111044016_Workers();
        mw_workers.setOffice(this);
        ms_name = "";
        ms_id = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * One Parameter Constructor. Creates empty HW01_111044016_Customers and HW01_111044016_Workers objects
     * and also assigns given name to name of Office.
     *
     * @param name Name of Office
     */
    public HW01_111044016_BankOffice(String name) {
        mc_customers = new HW01_111044016_Customers();
        mc_customers.setOffice(this);
        mw_workers = new HW01_111044016_Workers();
        mw_workers.setOffice(this);
        ms_name = name;
        ms_id = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * Two Parameter Constructor. Creates an empty HW01_111044016_Customers object and also
     * assigns given name to name of Office and HW01_111044016_Workers to HW01_111044016_Workers of Office.
     *
     * @param workers HW01_111044016_Workers of Office
     * @param name Name of Office
     */
    public HW01_111044016_BankOffice(String name, HW01_111044016_Workers workers) {
        mc_customers = new HW01_111044016_Customers();
        mc_customers.setOffice(this);
        mw_workers = workers;
        mw_workers.setOffice(this);
        ms_name = name;
        ms_id = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * Three parameter COnstructor. Takes name, customers and workers as
     * parameters.
     *
     * @param name Name of Office
     * @param workers HW01_111044016_Workers of Office
     * @param customers HW01_111044016_Customer of Office
     */
    public HW01_111044016_BankOffice(String name, HW01_111044016_Workers workers, HW01_111044016_Customers customers) {
        mc_customers = customers;
        mc_customers.setOffice(this);
        mw_workers = workers;
        mw_workers.setOffice(this);
        ms_name = name;
        ms_id = si_nextId.toString();
        ++si_nextId;
    }

    /**
     * Adds a new worker to Office
     *
     * @param worker New worker
     */
    public void addWorker(HW01_111044016_Worker worker) {
        mw_workers.add(worker);
    }

    /**
     * Adds a new customer to Office
     *
     * @param customer New customer
     */
    public void addCustomer(HW01_111044016_Customer customer) {
        mc_customers.add(customer);
    }

    /**
     * Returns HW01_111044016_Customers of Office
     *
     * @return HW01_111044016_Customers of Office
     */
    public HW01_111044016_Customers getCustomers() {
        return mc_customers;
    }

    /**
     * Returns name of Office
     *
     * @return Name of Office
     */
    public String getName() {
        return ms_name;
    }

    /**
     * Return HW01_111044016_Workers of Office
     *
     * @return HW01_111044016_Workers of Office
     */
    public HW01_111044016_Workers getWorkers() {
        return mw_workers;
    }

    /**
     * Removes Given HW01_111044016_Worker from Office then returns if
     *
     * @param worker HW01_111044016_Worker to remove
     * @return Removed HW01_111044016_Worker
     */
    public HW01_111044016_Worker removeWorker(HW01_111044016_Worker worker) {
        return mw_workers.remove(worker);
    }

    /**
     * Changes HW01_111044016_Customers of office to given HW01_111044016_Customers
     *
     * @param customers New list of HW01_111044016_Customers
     */
    public void setCustomers(HW01_111044016_Customers customers) {
        mc_customers = customers;
    }

    /**
     * Changes Name of Office with given one
     *
     * @param name new Name of Office
     */
    public void setName(String name) {
        ms_name = name;
    }

    /**
     * Changes HW01_111044016_Workers of Office with given HW01_111044016_Workers
     *
     * @param workers New list of HW01_111044016_Workers
     */
    public void setWorkers(HW01_111044016_Workers workers) {
        mw_workers = workers;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("## ");
        s.append(ms_name).append(" ##\n");
        s.append("Workers:\n-------\n").append(mw_workers);
        s.append("Customers:\n---------\n").append(mc_customers);

        return s.toString();
    }
}//end HW01_111044016_BankOffice