package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.Serializable;

/**
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public interface HW01_111044016_Person extends Serializable {

    /**
     * Gives HW01_111044016_Account of person
     *
     * @return HW01_111044016_Account of person
     */
    public HW01_111044016_Account getAccount();

    /**
     * Gives name of HW01_111044016_Person
     *
     * @return name of person
     */
    public String getName();

    /**
     * Changes HW01_111044016_Account of HW01_111044016_Person to given one
     *
     * @param account
     */
    public void setAccount(HW01_111044016_Account account);

    /**
     * Changes name of person with given one
     *
     * @param name
     */
    public void setName(String name);
}
