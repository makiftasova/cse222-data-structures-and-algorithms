package tr.edu.gyte.cse.makiftasova.cse222.hw01;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_Worker extends HW01_111044016_Customer {

    private HW01_111044016_BankOffice mo_office;

    /**
     * No parameter constructor. Assigns null to everything but HW01_111044016_Customer ID.
     */
    public HW01_111044016_Worker() {
        super();
        mo_office = null;
    }

    /**
     * One parameter constructor. Only takes name of HW01_111044016_Worker. Assigns null to
     * office and HW01_111044016_Account of worker
     *
     * @param name Name of HW01_111044016_Worker
     */
    @Deprecated
    public HW01_111044016_Worker(String name) {
        super(name);
        this.mo_office = null;
    }

    /**
     * Taken Name and HW01_111044016_Account of worker. Assigns null to its office
     *
     * @param account HW01_111044016_Account of HW01_111044016_Worker
     * @param name Name of HW01_111044016_Worker
     */
    @Deprecated
    public HW01_111044016_Worker(String name, HW01_111044016_Account account) {
        super(name, account);
        this.mo_office = null;
    }

    /**
     * Takes Name and Office of worker. Rest of fields are null.
     *
     * @param office Office of HW01_111044016_Worker
     * @param name Name of HW01_111044016_Worker
     */
    @Deprecated
    public HW01_111044016_Worker(String name, HW01_111044016_BankOffice office) {
        super(name);
        this.mo_office = office;
    }

    /**
     * Takes every single data about HW01_111044016_Worker.
     *
     * @param office Office of HW01_111044016_Worker
     * @param account HW01_111044016_Account of HW01_111044016_Worker
     * @param name Name of HW01_111044016_Worker
     */
    public HW01_111044016_Worker(String name, HW01_111044016_Account account, HW01_111044016_BankOffice office) {
        super(name, account);
        this.mo_office = office;
    }

    /**
     * Office of HW01_111044016_Worker
     *
     * @return office of HW01_111044016_Worker
     */
    public HW01_111044016_BankOffice getOffice() {
        return this.mo_office;
    }

    /**
     * Changes Office of HW01_111044016_Worker
     *
     * @param office New office of worker
     */
    public void setOffice(HW01_111044016_BankOffice office) {
        mo_office = office;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder(super.toString());
        s.append(" Ofice:").append(mo_office.getName());
        return s.toString();
    }
}//end HW01_111044016_Worker