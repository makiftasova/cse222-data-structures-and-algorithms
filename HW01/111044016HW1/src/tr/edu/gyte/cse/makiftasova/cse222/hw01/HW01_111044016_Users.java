package tr.edu.gyte.cse.makiftasova.cse222.hw01;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_Users extends Object {

    private ArrayList<HW01_111044016_User> ml_users;
    private final String mfs_fileName = "111044016HW01Users.bin";
    private HW01_111044016_BinaryOutput mbo_binOut;
    private HW01_111044016_BinaryInput mbi_binIn;

    /**
     * Generates an empty list of HW01_111044016_Users
     */
    public HW01_111044016_Users() {
        ml_users = new ArrayList<>();
    }

    /**
     * Adds given user to users list, if it is not presents in list
     *
     * @param user HW01_111044016_User to add list
     */
    public void addUser(HW01_111044016_User user) {
        for (HW01_111044016_User u : ml_users) {
            if (user.name().equals(u.name())) {
                return;
            }
        }
        ml_users.add(user);
    }

    /**
     * Removes given user from list and returns it
     *
     * @param user HW01_111044016_User to remove
     * @return Removed HW01_111044016_User
     */
    public HW01_111044016_User removeUser(HW01_111044016_User user) {
        return ml_users.remove(ml_users.indexOf(user));
    }

    /**
     * Writes Current Usre's data to File
     */
    public void saveToFile() {
        try {
            mbo_binOut = new HW01_111044016_BinaryOutput(mfs_fileName);
            for (HW01_111044016_User u : ml_users) {
                mbo_binOut.writeToFileFrom(u);
            }
            mbo_binOut.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace(System.err);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }

    }

    /**
     * Reads Backed up HW01_111044016_User data from file
     */
    public void loadFromFile() {
        try {
            mbi_binIn = new HW01_111044016_BinaryInput(mfs_fileName);
            Object tmp = null;
            while (null != (tmp = mbi_binIn.readFromFile())) {
                ml_users.add((HW01_111044016_User) tmp);
            }
            mbi_binIn.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace(System.err);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
    }

    /**
     * Returns member ArrayList<User> reference
     *
     * @return ArrayList<User> reference which contains users
     */
    public ArrayList<HW01_111044016_User> toArrayList() {
        return ml_users;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Users:\n-----\n");
        for (HW01_111044016_User u : ml_users) {
            s.append(u.toString()).append("\n");
        }
        return s.toString();
    }
}
