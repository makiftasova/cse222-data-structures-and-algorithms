package tr.edu.gyte.cse.makiftasova.cse222.hw01;

/**
 * CSE222 - HW01
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * @version 1.0
 * @created 16-Mar-2013 16:48:52
 */
public class HW01_111044016_UserInterface extends Object {

    /**
     * Main Menu function
     */
    public static void printMainMenu() {
        System.out.println("[1] Worker Access");
        System.out.println("[2] Customer Access");
        System.out.print("Enter Your Choice ==> ");
    }

    /**
     * Prints HW01_111044016_User Menu for given permissions
     *
     * @param userPermissions
     */
    public static void printOperationsMenu(int userPermissions) {
        switch (userPermissions) {
            case HW01_111044016_User.USER_ROOT: // If user is root
                System.out.println("[1] Office Operations");
                System.out.println("[2] User Operations");
            case HW01_111044016_User.USER_WORKER: // If user is a worker
                System.out.println("[3] Customer Operations");
                System.out.println("[4] Worker Operations");
            case HW01_111044016_User.USER_NORMAL: // If user is Customer
                System.out.println("[5] Add Funds");
                System.out.println("[6] Remove Funds");
                System.out.println("[X] Exit");
                System.out.print("Enter Your Choice ==> ");
                break;
            default:
                System.out.println("Illegal User ID: " + userPermissions);
                System.out.print("Please Contact With Your Software ");
                System.out.println("Department. Shutting Down...");
                System.exit(-1);
                break;
        }
    }

    /**
     * Takes Offices to manipulate.
     *
     * @param offices Bank Offices
     */
    public static void officeMenu(HW01_111044016_BankOffices offices) {
        System.out.println(offices);
        System.out.println("[1] ADD");
        System.out.println("[2] REMOVE");

    }
}
