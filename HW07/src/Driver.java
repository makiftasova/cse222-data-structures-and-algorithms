import java.util.Scanner;

import com.makiftasova.assignments.cse222.hw07.BinarySearchTree;

/**
 * CSE222 - HW07 - Driver -
 * 
 * Driver class of HW07. Contains some basic tests.
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public final class Driver {

	/**
	 * Well known main method.
	 * 
	 * @param args
	 *            Arguments from Command Line.
	 */
	public static void main(String[] args) {
		BinarySearchTree<Integer> bst = new BinarySearchTree<>(3);

		Integer toDelete = new Integer(4);

		bst.add(new Integer(8));
		bst.add(new Integer(3));
		bst.add(new Integer(10));
		bst.add(toDelete);
		bst.add(new Integer(6));
		bst.add(new Integer(5));
		bst.add(new Integer(7));
		bst.add(new Integer(2));
		bst.add(new Integer(9));
		bst.add(new Integer(1));
		bst.add(new Integer(11));
		bst.add(new Integer(0));

		System.out.println("Initial tree:\n-------------");
		System.out.println(bst.toString());

		System.out.println("INFO: Current size: " + bst.size());
		System.out.println("INFO: Current maxdepth: " + bst.maxDepth());

		System.out.println("\nINFO: Deleting item: " + toDelete);
		System.out.println("INFO: Result: " + bst.remove(toDelete));
		waitKey();

		System.out.println(bst.toString());
		System.out.println("INFO: Current size: " + bst.size());
		System.out.println("INFO: Current maxdepth: " + bst.maxDepth());

		System.out.println("\nINFO: Running command: clear()");
		bst.clear();

		waitKey();

		System.out.println(bst.toString());
		System.out.println("INFO: Current size: " + bst.size());
		System.out.println("INFO: Current maxdepth: " + bst.maxDepth());

	}

	/**
	 * Waits an input from user to continue
	 */
	@SuppressWarnings("resource")
	private static void waitKey() {
		System.out.println("\nMSG: Press [Enter] to print updated tree.");
		new Scanner(System.in).nextLine();
	}
}
