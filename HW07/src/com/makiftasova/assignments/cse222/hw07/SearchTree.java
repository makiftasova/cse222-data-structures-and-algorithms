package com.makiftasova.assignments.cse222.hw07;

/**
 * CSE222 - HW07 - SearchTree -
 * 
 * Interface to define a search tree
 * 
 * Student: Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 * @author Koffman and Wolfgang
 * */
public interface SearchTree<E extends Comparable<E>> {

	/**
	 * Inserts item where it belongs in the tree.
	 * 
	 * @param item
	 *            The item to be inserted
	 * @return <tt>true</tt> If the item is inserted, <tt>false</tt> if the item
	 *         was already in the tree.
	 */
	boolean add(E item);

	/**
	 * Determine if an item is in the tree
	 * 
	 * @param target
	 *            Item being sought in tree
	 * @return <tt>true</tt> If the item is in the tree, <tt>false</tt>
	 *         otherwise
	 */
	boolean contains(E target);

	/**
	 * Find an object in the tree
	 * 
	 * @param target
	 *            The item being sought
	 * @return A reference to the object in the tree that compares equal as
	 *         determined by compareTo to the target. If not found null is
	 *         returned.
	 */
	E find(E target);

	/**
	 * Removes target from tree.
	 * 
	 * @param target
	 *            Item to be removed
	 * @return A reference to the object in the tree that compares equal as
	 *         determined by compareTo to the target. If not found null is
	 *         returned.
	 * @post target is not in the tree
	 */
	E delete(E target);

	/**
	 * Removes target from tree.
	 * 
	 * @param target
	 *            Item to be removed
	 * @return <tt>true</tt> if the object was in the tree, <tt>false</tt>
	 *         otherwise
	 * @post target is not in the tree
	 */
	boolean remove(E target);

	/**
	 * Returns the maximum depth of the tree.
	 * 
	 * @return maximum depth of the tree.
	 */
	int maxDepth();
}
