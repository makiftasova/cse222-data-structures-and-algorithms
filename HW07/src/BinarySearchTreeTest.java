import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.makiftasova.assignments.cse222.hw07.BinarySearchTree;

/**
 * CSE222 - HW07 - BinarySearchTreeTest -
 * 
 * JUnit Test class of HW07. Contains some basic JUnit 4.x tests.
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public class BinarySearchTreeTest {

	static BinarySearchTree<Integer> bst;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		bst = new BinarySearchTree<>(3);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		bst = null;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		bst.clear();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		bst.clear();
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#add(java.lang.Comparable)}
	 * .
	 */
	@Test
	public void testAdd() {
		bst.add(new Integer(13));
		assertEquals("Size: ", 1, bst.size());
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#add(java.lang.Comparable)}
	 * .
	 */
	@Test
	public void testAdd2() {
		bst.add(new Integer(13));
		bst.add(new Integer(22));
		bst.add(new Integer(43));
		bst.add(new Integer(13));
		assertEquals("Size: ", 3, bst.size());
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#contains(java.lang.Comparable)}
	 * .
	 */
	@Test
	public void testContains() {
		bst.add(new Integer(13));
		bst.add(new Integer(22));
		bst.add(new Integer(43));
		assertTrue("Has 22", bst.contains(new Integer(22)));
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#find(java.lang.Comparable)}
	 * .
	 */
	@Test
	public void testFind() {
		Integer i = new Integer(36);
		bst.add(new Integer(13));
		bst.add(new Integer(22));
		bst.add(new Integer(43));
		bst.add(i);
		assertEquals("Find " + i, i, bst.find(i));
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#find(java.lang.Comparable)}
	 * .
	 */
	@Test
	public void testFind2() {
		Integer i = new Integer(36);
		bst.add(new Integer(13));
		bst.add(new Integer(22));
		bst.add(new Integer(43));
		assertNotSame("Find " + i, i, bst.find(i));
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#delete(java.lang.Comparable)}
	 * .
	 */
	@Test
	public void testDelete() {
		Integer toDelete = new Integer(4);
		bst.add(new Integer(8));
		bst.add(new Integer(3));
		bst.add(new Integer(10));
		bst.add(toDelete);
		bst.add(new Integer(6));
		bst.add(new Integer(5));
		bst.add(new Integer(7));
		bst.add(new Integer(2));
		bst.add(new Integer(9));
		bst.add(new Integer(1));
		bst.add(new Integer(11));
		bst.add(new Integer(0));

		assertEquals("Delete " + toDelete, toDelete, bst.delete(toDelete));
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#remove(java.lang.Comparable)}
	 * .
	 */
	@Test
	public void testRemove() {
		Integer toRemove = new Integer(4);
		bst.add(new Integer(8));
		bst.add(new Integer(3));
		bst.add(new Integer(10));
		bst.add(toRemove);
		bst.add(new Integer(6));
		bst.add(new Integer(5));
		bst.add(new Integer(7));
		bst.add(new Integer(2));
		bst.add(new Integer(9));
		bst.add(new Integer(1));
		bst.add(new Integer(11));
		bst.add(new Integer(0));

		assertTrue("Remove " + toRemove, bst.remove(toRemove));
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#remove(java.lang.Comparable)}
	 * .
	 */
	@Test
	public void testRemove2() {
		Integer toRemove = new Integer(4);
		bst.add(new Integer(8));
		bst.add(new Integer(3));
		bst.add(new Integer(10));
		bst.add(new Integer(6));
		bst.add(new Integer(5));
		bst.add(new Integer(7));
		bst.add(new Integer(2));
		bst.add(new Integer(9));
		bst.add(new Integer(1));
		bst.add(new Integer(11));
		bst.add(new Integer(0));

		assertFalse("Remove " + toRemove, bst.remove(toRemove));
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#maxDepth()}
	 * .
	 */
	@Test
	public void testMaxDepth() {
		bst.add(new Integer(0));
		bst.add(new Integer(1));
		bst.add(new Integer(2));
		assertEquals("Max Depth: ", 2, bst.maxDepth());
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#maxDepth()}
	 * .
	 */
	@Test
	public void testMaxDepth2() {
		bst.add(new Integer(1));
		bst.add(new Integer(0));
		bst.add(new Integer(2));
		assertEquals("Max Depth: ", 1, bst.maxDepth());
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#isLeaf()}
	 * .
	 */
	@Test
	public void testIsLeaf() {
		bst.add(new Integer(5));
		assertTrue("Is Laeaf Tree: ", bst.isLeaf());
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#isLeaf()}
	 * .
	 */
	@Test
	public void testIsLeaf2() {
		bst.add(new Integer(5));
		bst.add(new Integer(4));
		bst.add(new Integer(6));
		bst.add(new Integer(12));
		assertFalse("Is Laeaf Tree: ", bst.isLeaf());
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#isEmpty()}
	 * .
	 */
	@Test
	public void testIsEmpty() {
		assertTrue("Is empty: ", bst.isEmpty());
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#isEmpty()}
	 * .
	 */
	@Test
	public void testIsEmpty2() {
		bst.add(new Integer(1234));
		assertFalse("Is empty: ", bst.isEmpty());
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#clear()}.
	 */
	@Test
	public void testClear() {
		bst.add(new Integer(12));
		bst.add(new Integer(31));
		bst.add(new Integer(43));
		bst.add(new Integer(51));
	}

	/**
	 * Test method for
	 * {@link com.makiftasova.assignments.cse222.hw07.BinarySearchTree#size()}.
	 */
	@Test
	public void testSize() {
		int expected = 4;
		for (int i = 0; i < expected; ++i) {
			bst.add(new Integer(i));
		}

		assertEquals("Size: ", expected, bst.size());

	}

}
