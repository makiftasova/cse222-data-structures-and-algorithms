package tr.edu.gyte.cse.cse222.makiftasova.hw04;

import java.util.*;

/**
 * 
 * CSE222 - HW04 - GITLanguage - Class that can evaluate a postfix expression.
 * This class completely written by Koffman & Wolfgang except
 * "int powInt(int a, int b)" method and support for operator '^'.
 * 
 * Student: Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 * @author Koffman & Wolfgang
 * 
 * */

public class IntegerPostfixEvaluator {

	/**
	 * Error sting for Stack Empty error
	 */
	public static final String STACK_EMPTY_ERR = "The stack is empty";

	/**
	 * Error string for Stack is not Empty error
	 */
	public static final String STACK_NOT_EMPTY_ERR = "Stack should be empty";

	/**
	 * Error string gor stack is not empty error
	 */
	public static final String INVALID_CHAR_ERR = "Invalid character encountered: ";

	// Nested Class
	/**
	 * Class to report a syntax error when evaluating a postfix expression.
	 */
	public static class EvalErrorException extends Exception {

		private static final long serialVersionUID = 7742353268387685253L;

		/**
		 * Construct a SyntaxError with the specified message.
		 * 
		 * @param message
		 *            The message
		 */
		EvalErrorException(String message) {
			super(message);
		}
	}

	// Constant
	/**
	 * A list of operators.
	 */
	private static final String OPERATORS = "+-*/^";

	// Data Field
	/**
	 * The operand stack.
	 */
	private Stack<Integer> operandStack;

	// Methods
	/**
	 * Evaluates the current operation. This function pops the two operands off
	 * the operand stack and applies the operator.
	 * 
	 * @param op
	 *            A character representing the operator
	 * @return The result of applying the operator
	 * @throws EmptyStackException
	 *             if pop is attempted on an empty stack
	 */
	private int evalOp(char op) {
		// Pop the two operands off the stack.
		int rhs = operandStack.pop();
		int lhs = operandStack.pop();
		int result = 0;
		// Evaluate the operator.
		switch (op) {
		case '+':
			result = lhs + rhs;
			break;
		case '-':
			result = lhs - rhs;
			break;
		case '/':
			result = lhs / rhs;
			break;
		case '*':
			result = lhs * rhs;
			break;
		case '^':
			result = powInt(lhs, rhs);
			break;

		}
		return result;
	}

	/**
	 * Calculates a^b for int type. This function defined because of pow method
	 * from {@link Math} does not usable with type int.
	 * 
	 * @param a
	 *            Base value
	 * @param b
	 *            Power
	 * @return a^b
	 */
	private int powInt(int a, int b) {
		int result = 1;

		for (int i = 0; i < b; ++i) {
			result *= a;
		}

		return result;
	}

	/**
	 * Determines whether a character is an operator.
	 * 
	 * @param ch
	 *            The character to be tested
	 * @return true if the character is an operator
	 */
	private boolean isOperator(char ch) {
		return OPERATORS.indexOf(ch) != -1;
	}

	/**
	 * Evaluates a postfix expression.
	 * 
	 * @param expression
	 *            The expression to be evaluated
	 * @return The value of the expression
	 * @throws SyntaxError
	 *             if a syntax error is detected
	 */
	public int eval(String expression) throws EvalErrorException {
		// Create an empty stack.
		operandStack = new Stack<Integer>();

		// Process each token.
		StringTokenizer tokens = new StringTokenizer(expression);
		try {
			while (tokens.hasMoreTokens()) {
				String nextToken = tokens.nextToken();

				// Does it start with a digit?
				if (Character.isDigit(nextToken.charAt(0))) {
					// Get the integer value.
					int value = Integer.parseInt(nextToken);
					// Push value onto operand stack.
					operandStack.push(value);
				} // Is it an operator?
				else if (isOperator(nextToken.charAt(0))) {
					// Evaluate the operator.
					int result = evalOp(nextToken.charAt(0));
					// Push result onto the operand stack.
					operandStack.push(result);
				} else {
					// Invalid character.
					throw new EvalErrorException(INVALID_CHAR_ERR + nextToken);
				}
			} // End while.

			// No more tokens - pop result from operand stack.
			int answer = operandStack.pop();
			// Operand stack should be empty.
			if (operandStack.empty()) {
				return answer;
			} else {
				// Indicate syntax error.
				throw new EvalErrorException(STACK_NOT_EMPTY_ERR);
			}
		} catch (EmptyStackException ex) {
			// Pop was attempted on an empty stack.
			throw new EvalErrorException(STACK_EMPTY_ERR);
		}
	}
}
