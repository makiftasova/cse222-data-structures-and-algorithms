package tr.edu.gyte.cse.cse222.makiftasova.hw04;

import java.util.ListIterator;
import java.util.StringTokenizer;

import tr.edu.gyte.cse.cse222.makiftasova.hw04.FloatPostfixEvaluator.FloatEvalErrorException;
import tr.edu.gyte.cse.cse222.makiftasova.hw04.InfixToPostfix.ConvertErrorException;
import tr.edu.gyte.cse.cse222.makiftasova.hw04.IntegerPostfixEvaluator.EvalErrorException;
import tr.edu.gyte.cse.cse222.makiftasova.hw04.Type.UnknownTypeException;
import tr.edu.gyte.cse.cse222.makiftasova.hw04.Variable.DataTypeException;

/**
 * CSE222 - HW04 - GITLanguage - Interpreter class. Reads all commands from
 * source file, then interprets it. When a new varaible declared, it value is
 * 0(zero) by default.
 * 
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public class GITInterpreter {

	/**
	 * List of supported operators by GITLanguage
	 */
	private static final String OPERATORS = "+-*/=^";

	/**
	 * Regular expression for splitting lines from Whitespace characters
	 */
	private static final String SPLIT_REGEX = "\\s";

	private FileReader m_file;
	private ListIterator<String> m_lines;
	private Variables m_vars;
	private int m_lineNumber;
	private int m_currLineNum;

	public GITInterpreter(String FileName) {
		m_file = new FileReader(FileName);
		m_lines = m_file.getListIterator();
		m_vars = new Variables();
		m_lineNumber = m_file.numberOfLines();
		m_currLineNum = 0;
	}

	/**
	 * Evaluates currently open source file.
	 * 
	 * @throws SyntaxErrorException
	 *             If there is any Syntax Error happens
	 * @throws UnknownTypeException
	 *             If there is any type code missmatch happens
	 */
	public void eval() throws SyntaxErrorException, UnknownTypeException,
			MixedTypeException {

		String currLine = "";
		String tmpVarName = "";
		String lineBegin = "";

		while (m_currLineNum < m_lineNumber) {

			currLine = m_lines.next();

			String toCheck = new String(currLine);

			if (isMixedType(toCheck)) {
				throw new MixedTypeException(m_currLineNum);
			}

			// Fix possible brackets related syntax errors
			currLine = currLine.replace("(", " ( ");
			currLine = currLine.replace(")", " ) ");

			// Fix possible operator related syntax errors
			currLine = currLine.replace("=", " = ");
			currLine = currLine.replace("+", " + ");
			currLine = currLine.replace("-", " - ");
			currLine = currLine.replace("*", " * ");
			currLine = currLine.replace("/", " / ");
			currLine = currLine.replace("^", " ^ ");

			// If line is empty
			if (currLine.equals("") || currLine.equals("\n")
					|| currLine.equals(" ")) {
				++m_lineNumber;
				continue;
			}

			lineBegin = currLine.trim().split(SPLIT_REGEX)[0];

			// check for variable definition
			if (lineBegin.equals(Keyword.INT)) {
				// variable type is int
				tmpVarName = currLine.trim().split(SPLIT_REGEX)[1];
				try {
					if (!m_vars.add(new Variable(Type.INT, tmpVarName,
							new Integer(0)))) {
						throw new SyntaxErrorException(m_currLineNum,
								"Redefinition of variable " + tmpVarName);
					}

				} catch (ClassCastException | DataTypeException e) {
					System.out.println(e.getMessage());
				}

				if ((currLine.trim().split(SPLIT_REGEX).length) > 2) {
					throw new SyntaxErrorException(m_currLineNum,
							"Too Much Operands for varaible declaration");
				}

			} else if (lineBegin.equals(Keyword.FLOAT)) {
				tmpVarName = currLine.trim().split(SPLIT_REGEX)[1];

				try {
					if (!m_vars.add(new Variable(Type.FLOAT, tmpVarName,
							new Float(0.0)))) {
						throw new SyntaxErrorException(m_currLineNum,
								"Redefinition of variable " + tmpVarName);
					}
				} catch (ClassCastException | DataTypeException e) {
					System.out.println(e.getMessage());
				}

				if ((currLine.trim().split(SPLIT_REGEX).length) > 2) {
					throw new SyntaxErrorException(m_currLineNum,
							"Too Much Operands for varaible declaration");
				}

			} else if (lineBegin.equals(Keyword.PRINT)) {

				tmpVarName = currLine.trim().split(" ")[1];

				if (!m_vars.hasVariable(tmpVarName)) {
					throw new SyntaxErrorException(m_currLineNum, "Variable "
							+ tmpVarName + " is not declared");
				}

				if ((currLine.trim().split(SPLIT_REGEX).length) > 2) {
					throw new SyntaxErrorException(m_currLineNum,
							"Too Much Operands for \"print\"");
				}

				System.out.println(m_vars.get(tmpVarName).getValue());

			} else if (m_vars.hasVariable(lineBegin)) {
				Variable toAssign = m_vars.get(lineBegin);

				// Get operator
				String op = "=";

				if (!currLine.contains(op)) {
					throw new SyntaxErrorException(m_currLineNum, "variable \""
							+ lineBegin + "\" used as Lvalue of "
							+ "a non-assignment operator");
				}

				StringTokenizer lineTest = new StringTokenizer(currLine);
				String currToken = "";
				int tokenCount = 0;

				while (lineTest.hasMoreTokens()) {
					++tokenCount;
					currToken = lineTest.nextToken();

					if ((2 == tokenCount) && (!currToken.equals("="))) {
						throw new SyntaxErrorException(m_currLineNum,
								"Missing operator \"=\"");
					}
				}

				// Get expression
				String exp = "";
				try {
					exp = currLine.trim().substring(currLine.indexOf(op) + 1);
				} catch (StringIndexOutOfBoundsException e) {
					throw new SyntaxErrorException(m_currLineNum,
							"Invalid Syntax");
				}

				if (op.length() > 1) {
					throw new SyntaxErrorException(m_currLineNum,
							"Unknown operator " + op);
				}

				if (OPERATORS.contains(op)) {
					if (toAssign.getType() == Type.INT) {

						for (Variable var : m_vars) {
							if (Type.INT == var.getType()) {
								Integer i = (Integer) var.getValue();
								exp = exp.replace(var.getName(), i.toString());
							}

						}

						Integer result = (Integer) toAssign.getValue();

						result = (Integer) parseExp(exp, Type.INT,
								m_currLineNum);

						toAssign.setValue(result);
					} else {

						for (Variable var : m_vars) {
							if (Type.FLOAT == var.getType()) {
								Float f = (Float) var.getValue();
								exp = exp.replace(var.getName(), f.toString());
							}

						}

						Float result = (Float) toAssign.getValue();

						result = (Float) parseExp(exp, Type.FLOAT,
								m_currLineNum);

						toAssign.setValue(result);
					}

				} else {

					throw new SyntaxErrorException(m_currLineNum,
							"Unknown operator " + op);
				}

			} else {

				if (lineBegin.equals("=")) {
					throw new SyntaxErrorException(m_currLineNum,
							"Assignment operator has no LValue");
				}
				throw new SyntaxErrorException(m_currLineNum, "\"" + lineBegin
						+ "\" is not defined");
			}

			++m_currLineNum;

		}

	}

	/**
	 * Parses given expression then calls related evaluator class to evaluate
	 * it.
	 * 
	 * @param line
	 *            Expression to evaluate
	 * 
	 * @return Result of Expression
	 * 
	 * @throws SyntaxErrorException
	 *             If there is any Syntax Error happens
	 * @throws UnknownTypeException
	 *             If there is any type code missmatch happens
	 */
	private Number parseExp(String line, int type, int lineNo)
			throws SyntaxErrorException, UnknownTypeException {

		if (Type.INT == type) {

			InfixToPostfix coverter = new InfixToPostfix();
			IntegerPostfixEvaluator evaluator = new IntegerPostfixEvaluator();
			Integer result = null;
			try {
				String postfix = coverter.convert(line);
				result = evaluator.eval(postfix);

			} catch (ConvertErrorException e) {
				throw new SyntaxErrorException(lineNo, e.getMessage());
			} catch (EvalErrorException e) {

				String msg = "";

				if (e.getMessage().equals(
						IntegerPostfixEvaluator.STACK_EMPTY_ERR)) {

					msg = "Missing operand at expression";
				} else if (e.getMessage().equals(
						IntegerPostfixEvaluator.STACK_NOT_EMPTY_ERR)) {

					msg = "Too many operands for expression";
				} else {
					msg = e.getMessage();

				}

				throw new SyntaxErrorException(lineNo, msg);
			}

			return result;

		} else if (Type.FLOAT == type) {
			InfixToPostfix converter = new InfixToPostfix();
			FloatPostfixEvaluator evaluator = new FloatPostfixEvaluator();
			Float result = null;

			try {
				String postfix = converter.convert(line);
				result = evaluator.eval(postfix);

			} catch (ConvertErrorException e) {
				throw new SyntaxErrorException(lineNo, e.getMessage());
			} catch (FloatEvalErrorException e) {

				String msg = "";

				if (e.getMessage()
						.equals(FloatPostfixEvaluator.STACK_EMPTY_ERR)) {

					msg = "Missing operand at expression";
				} else if (e.getMessage().equals(
						FloatPostfixEvaluator.STACK_NOT_EMPTY_ERR)) {

					msg = "Too many operands for expression";
				} else {
					msg = e.getMessage();
				}

				throw new SyntaxErrorException(lineNo, msg);
			}

			return result;
		}

		throw new UnknownTypeException(type);

	}

	/**
	 * Checks given expression. If expression is a mixed type expresion
	 * (contains both float and int variables) returns <tt>true</tt>, else
	 * returns <tt>false</tt>
	 * 
	 * @param exp
	 *            Expression to check
	 * @return If expression is a mixed type expression returns <tt>true</tt>,
	 *         else returns <tt>false</tt>
	 */
	private boolean isMixedType(String exp) {

		String orig = new String(exp);
		boolean hasInt = false;
		boolean hasFloat = false;

		for (Variable var : m_vars) {
			if (var.getType() == Type.INT) {
				exp = exp.replace(var.getName(), var.getValue().toString());
			}
		}

		if (!exp.equals(orig)) {
			hasInt = true;
		}

		orig = new String(exp);

		for (Variable var : m_vars) {
			if (var.getType() == Type.FLOAT) {
				exp = exp.replace(var.getName(), var.getValue().toString());
			}
		}

		if (!exp.equals(orig)) {
			hasFloat = true;
		}

		return (hasFloat && hasInt);
	}

	/**
	 * CSE222 - HW04 - GITLanguage - Syntax Error Exception Class of
	 * GITInterpreter. This exception will be thrown when there is a Syntax
	 * Error in source code.
	 * 
	 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number:
	 *         111044016
	 */
	public static final class SyntaxErrorException extends Exception {

		private static final long serialVersionUID = 847185371220819767L;

		public SyntaxErrorException(int lineNumber, String message) {
			super("Syntax Error in line #" + (lineNumber + 1) + ": " + message);
		}
	}

	/**
	 * CSE222 - HW04 - GITLanguage - Mixed Type Exception Class of
	 * GITInterpreter. This exception will be thrown when there is an operation
	 * with mixed type.
	 * 
	 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number:
	 *         111044016
	 */
	public static final class MixedTypeException extends Exception {

		private static final long serialVersionUID = 56286718524605835L;

		public MixedTypeException(int lineNumber) {
			super("Mixed type operation in line #" + (lineNumber + 1));
		}
	}

}