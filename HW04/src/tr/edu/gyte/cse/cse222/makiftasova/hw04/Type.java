package tr.edu.gyte.cse.cse222.makiftasova.hw04;

/**
 * 
 * CSE222 - HW04 - GITLanguage -
 * 
 * Type codes of GITLanguage.
 * 
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public interface Type {

	/**
	 * Type code of int
	 */
	public static final int INT = 1;

	/**
	 * Type code of float
	 */
	public static final int FLOAT = 2;

	/**
	 * Type code for commands like print
	 */
	public static final int COMMAND = 3;

	/**
	 * 
	 * CSE222 - HW04 - GITLanguage - An exception which used when there is a
	 * corrupt data type found on source code of GITLanguage files (.git).
	 * 
	 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number:
	 *         111044016
	 * 
	 */
	public static final class UnknownTypeException extends Exception {

		private static final long serialVersionUID = -2754635106718213966L;

		public UnknownTypeException(int typeCode) {
			super("Unknown Type Code: " + typeCode);
		}
	}

}
