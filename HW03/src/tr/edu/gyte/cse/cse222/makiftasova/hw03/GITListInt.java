package tr.edu.gyte.cse.cse222.makiftasova.hw03;

import java.util.ListIterator;

/**
 * 
 * CSE222 - HW03 - GITListInt.java --  
 * 
 * An interface for adding required functions for homework to Collection
 * interface.
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * 
 *         Student Number: 111044016
 * 
 * @param <E>
 *            Type of collection
 */
public interface GITListInt<E> extends Iterable<E> {

	/**
	 * Returns total number of elements which contained in list
	 * 
	 * @return Total number of elements in list
	 */
	public int size();

	/**
	 * Returns <tt>true</tt> if list is empty, else return <tt>false</tt>
	 * 
	 * @return <tt>true</tt> if list is empty, else return <tt>false</tt>
	 */
	public boolean isEmpty();

	/**
	 * Add an item at the specified index.
	 * 
	 * @param index
	 *            Index to add.
	 * @param obj
	 *            Object to add.
	 */
	public void add(int index, E obj) throws IndexOutOfBoundsException;

	/**
	 * Insert an object at the beginning of the list.
	 * 
	 * @param item
	 *            Item to insert.
	 */
	public void addFirst(E item);

	/**
	 * Insert an object at the end of the list.
	 * 
	 * @param item
	 *            Item to insert.
	 */
	public void addLast(E item);

	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element.
	 * 
	 * @param index
	 *            Index of element to change
	 * @param element
	 *            New element
	 * 
	 * @return Newly Inserted element
	 */
	public E set(int index, E element);

	/**
	 * Get the element at position index.
	 * 
	 * @param index
	 *            Index of element.
	 * 
	 * @return Returns element at given index, if given index is out of bounds
	 *         throws exception.
	 * 
	 * @throws ArrayIndexOutOfBoundsException
	 *             If given index is greater than size of GITList or index is
	 *             negative.
	 */
	public E get(int index) throws IndexOutOfBoundsException;

	/**
	 * Get the first element in the list.
	 * 
	 * @return Returns first element of list if there is no element in list,
	 *         returns <tt>null</tt>.
	 */
	public E getFirst();

	/**
	 * Get the last element in the list.
	 * 
	 * @return Returns last element of list if there is no element in list,
	 *         returns <tt>null</tt>.
	 */
	public E getLast();

	/**
	 * Return a ListIterator to the list
	 * 
	 * @return a ListIterator to the list
	 */
	public ListIterator<E> listIterator();

	/**
	 * Return a ListIterator that begins at index
	 * 
	 * @param index
	 *            Beginnig of ListIterator
	 * @return a ListIterator that begins at index
	 */
	public ListIterator<E> listIterator(int index);

}
