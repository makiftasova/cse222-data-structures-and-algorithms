/**
 * 
 */
package tr.edu.gyte.cse.cse222.makiftasova.hw03;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Test;

/**
 * 
 * CSE222 - HW03 - GITListInt.java --
 * 
 * JUnit Tests of HW03
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * 
 *         Student Number: 111044016
 * 
 */
public class GITListTest {

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#size()}.
	 */
	@Test
	public void testSize() {
		GITList<Integer> list = new GITList<>();

		int i = 0;
		int numOfItems = 20;

		for (i = 0; i < numOfItems; ++i) {
			list.addFirst(i);
		}

		int expected = numOfItems;
		int result = list.size();
		assertEquals(expected, result);
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#isEmpty()}.
	 */
	@Test
	public void testIsEmpty() {
		GITList<Integer> list = new GITList<>();
		assertTrue("Eror in method list.isEmpty()", list.isEmpty());
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#isEmpty()}.
	 */
	@Test
	public void testIsEmpty2() {
		GITList<Integer> list = new GITList<>();
		list.addLast(3);
		assertFalse("Eror in method list.isEmpty()", list.isEmpty());
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#add(int, java.lang.Object)}
	 * .
	 */
	@Test
	public void testAdd() {
		GITList<Integer> list = new GITList<>();

		int i = 0;
		int index = 4;

		for (i = 0; i < index; ++i) {
			list.addLast(i);
		}

		list.add(index, i);

		int expected = i;
		int result = list.get(index);
		assertEquals(expected, result);
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#addFirst(java.lang.Object)}
	 * .
	 */
	@Test
	public void testAddFirst() {
		GITList<Integer> list = new GITList<>();
		int itemToAdd = 4;

		for (int i = 0; i < 5; ++i) {
			list.addLast(i);
		}

		list.addFirst(itemToAdd);

		int expected = itemToAdd;
		int result = list.get(0);
		assertEquals(expected, result);
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#addLast(java.lang.Object)}
	 * .
	 */
	@Test
	public void testAddLast() {
		GITList<Integer> list = new GITList<>();
		int itemToAdd = 4;

		for (int i = 0; i < 5; ++i) {
			list.addLast(i);
		}

		list.addLast(itemToAdd);

		int expected = itemToAdd;
		int result = list.get(list.size() - 1);
		assertEquals(expected, result);
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#set(int, java.lang.Object)}
	 * .
	 */
	@Test
	public void testSet() {
		GITList<Integer> list = new GITList<>();

		int indexToChange = 8;
		for (int i = 0; i < (indexToChange + 5); ++i) {
			list.addLast(i);
		}

		int itemToSet = (indexToChange + 12);

		int expected = itemToSet;
		int result = list.set(indexToChange, itemToSet);
		assertEquals(expected, result);
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#get(int)}.
	 */
	@Test
	public void testGet() {
		GITList<Integer> list = new GITList<>();

		int i = 0;
		int index = 4;

		for (i = 0; i < index; ++i) {
			list.addLast(i);
		}

		list.add(index, i);

		int expected = i;
		int result = list.get(index);
		assertEquals(expected, result);
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#getFirst()}.
	 */
	@Test
	public void testGetFirst() {
		GITList<Integer> list = new GITList<>();
		int itemToAdd = 4;

		for (int i = 0; i < 5; ++i) {
			list.addLast(i);
		}

		list.addFirst(itemToAdd);

		int expected = itemToAdd;
		int result = list.getFirst();
		assertEquals(expected, result);
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#getLast()}.
	 */
	@Test
	public void testGetLast() {
		GITList<Integer> list = new GITList<>();
		int itemToAdd = 4;

		for (int i = 0; i < 5; ++i) {
			list.addLast(i);
		}

		list.addLast(itemToAdd);

		int expected = itemToAdd;
		int result = list.getLast();
		assertEquals(expected, result);
	}

	/**
	 * Test method for
	 * {@link tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList#iterator()}.
	 */
	@Test
	public void testIterator() {
		GITList<Integer> list = new GITList<>();

		int i = 0;
		int numOfElements = 4;

		for (i = 0; i < numOfElements; ++i) {
			list.addLast(i);
		}

		int count = 0;
		Iterator<Integer> iter = list.iterator();
		Integer testVal = new Integer(0);

		for (count = 0; iter.hasNext(); ++count) {
			testVal = iter.next();
			if ((false == iter.hasNext()) && (null != testVal)) {
				++count;
			}
		}

		int expected = numOfElements;
		int result = count;
		assertEquals(expected, result);
	}

}
