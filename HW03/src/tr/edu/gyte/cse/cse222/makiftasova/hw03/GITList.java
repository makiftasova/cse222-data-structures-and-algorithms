package tr.edu.gyte.cse.cse222.makiftasova.hw03;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * CSE222 - HW03 - GITList.java --
 * 
 * A kind of Linked List implementation which can provide Collection Iterators.
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * 
 *         Student Number: 111044016
 * 
 * @param <E>
 *            Type of collection.
 */
public class GITList<E> implements GITListInt<E> {

	/**
	 * Current size of List.
	 */
	private int size = 0;
	/**
	 * Maximum array capacity per node.
	 */
	private int maxArrayCapacity = 10;

	/**
	 * Current number of nodes.
	 */
	private int numOfNodes = 0;

	/**
	 * First node.
	 */
	Node head = null;

	/**
	 * Last node.
	 */
	Node tail;

	/**
	 * Creates a GITList which keeps <tt>10</tt> elements per node.
	 */
	public GITList() {
		size = 0;
		maxArrayCapacity = 10;
		head = new Node();
		tail = head;
		++numOfNodes;
	}

	/**
	 * Creates a GITList which keeps <tt>capacity</tt> elements per node.
	 * 
	 * @param capacity
	 *            Number of elements per node.
	 */
	public GITList(int capacity) {
		size = 0;
		maxArrayCapacity = capacity;
		head = new Node();
		tail = head;
		++numOfNodes;
	}

	/**
	 * Returns total number of elements which contained in list
	 * 
	 * @return Total number of elements in list
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Returns <tt>true</tt> if list is empty, else return <tt>false</tt>
	 * 
	 * @return <tt>true</tt> if list is empty, else return <tt>false</tt>
	 */
	@Override
	public boolean isEmpty() {
		return (0 == size);
	}

	/**
	 * Add an item at the specified index. At first checks for is index is
	 * acceptable then tries to find which node contains indexth element of
	 * list. After found node contains indexth element, jumps to it then calls
	 * {@link GITList#insertIntoNode(Node, int, Object)}
	 * 
	 * @param index
	 *            Index to add.
	 * @param obj
	 *            Object to add.
	 */
	@Override
	public void add(int index, E obj) throws IndexOutOfBoundsException {

		if ((index > size) || (index < 0)) {
			throw new IndexOutOfBoundsException("There is no such index "
					+ index);
		}

		if (index == size) {
			addLast(obj);
			return;
		}

		int timesOfJumps = (index / maxArrayCapacity);

		if (0 == timesOfJumps) {
			insertIntoNode(head, index, obj);
			return;
		}

		Node curr = head;

		for (int i = 0; i < timesOfJumps; ++i) {
			curr = curr.next;
			/* calculate index of requested item */
			index -= maxArrayCapacity;
		}

		insertIntoNode(curr, index, obj);
		return;
	}

	/**
	 * Insert an object at the beginning of the list. Just calls
	 * {@link GITList#insertIntoNode(Node, int, Object)} as insertIntoNode(head,
	 * 0, item)
	 * 
	 * @param item
	 *            Item to insert
	 */
	@Override
	public void addFirst(E item) {
		insertIntoNode(head, 0, item);
		return;
	}

	/**
	 * Insert an object at the end of the list. First checks is last node has
	 * enough space for a new item, if not created a new node as new tail. After
	 * there is enough space calls
	 * {@link GITList#insertIntoNode(Node, int, Object)} to insert item into
	 * last position of tail node
	 * 
	 * @param item
	 *            Item to insert
	 */
	@Override
	public void addLast(E item) {

		if (tail.size == maxArrayCapacity) {
			Node newTail = new Node();
			tail.next = newTail;
			newTail.prev = tail;
			newTail.next = null;
			tail = newTail;
		}

		insertIntoNode(tail, tail.size, item);
		return;
	}

	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element. Jumps through nodes until it finds indexth element of
	 * list. After found it, changes it with given new one.
	 * 
	 * @param index
	 *            Index of element to change
	 * @param element
	 *            New element
	 * 
	 * @return Newly Inserted element
	 */
	@Override
	public E set(int index, E element) {

		if ((index > size) || (index < 0)) {
			throw new IndexOutOfBoundsException("There is no such index "
					+ index);
		}

		int timesOfJumps = (index / maxArrayCapacity);

		if (0 == timesOfJumps) {
			return (head.elements[index] = element);
		}

		Node curr = head;

		for (int i = 0; i < timesOfJumps; ++i) {
			curr = curr.next;
			/* calculate index of requested item */
			index -= maxArrayCapacity;
		}

		return (curr.elements[index] = element);
	}

	/**
	 * Get the element at position index. Jumps through nodes until finds
	 * indexth element of list, then returns it if given index is not an
	 * illegal value
	 * 
	 * @param index
	 *            Index of element to get
	 * 
	 * @return Element at given index
	 * 
	 * @throws IndexOutOfBoundsException
	 *             If given index is out of list
	 */
	@Override
	public E get(int index) throws IndexOutOfBoundsException {

		if ((index >= size) || (index < 0)) {
			throw new IndexOutOfBoundsException("There is no such index "
					+ index);
		}

		int timesOfJumps = (index / maxArrayCapacity);

		if (0 == timesOfJumps) {
			return head.elements[index];
		}

		Node curr = head;

		/* find node which contains requested index */
		for (int i = 0; i < timesOfJumps; ++i) {
			curr = curr.next;
			/* calculate index of requested item */
			index -= maxArrayCapacity;
		}

		return (curr.elements[index]);
	}

	/**
	 * Get the first element in the list. Returns first element of array which
	 * stored in first node of list
	 * 
	 * @return Returns first element of list if there is no element in list,
	 *         returns <tt>null</tt>.
	 */
	@Override
	public E getFirst() {
		return head.elements[0];
	}

	/**
	 * Get the last element in the list. returns tails node's array's (nodesize
	 * - 1) element
	 * 
	 * @return Returns last element of list if there is no element in list,
	 *         returns <tt>null</tt>.
	 */
	@Override
	public E getLast() {
		return tail.elements[tail.size - 1];
	}

	/**
	 * Return an Iterator to the list Simply calls inner Iterator class'
	 * Constructor
	 * 
	 * @return an Iterator to the list
	 */
	@Override
	public Iterator<E> iterator() {
		return new GITListIterator();
	}

	/**
	 * Return a ListIterator to the list
	 * 
	 * @return a ListIterator to the list
	 */
	// @Override
	public ListIterator<E> listIterator() {
		return new GITListIterator();
	}

	/**
	 * Return a ListIterator that begins at index
	 * 
	 * @param index
	 *            Beginnig of ListIterator
	 * @return a ListIterator that begins at index
	 */
	// @Override
	public ListIterator<E> listIterator(int index) {
		if ((index >= size) || (index < 0)) {
			throw new IndexOutOfBoundsException("There is no such index "
					+ index);
		}

		int timesOfJumps = (index / maxArrayCapacity);

		Node curr = head;
		int pos = index;

		/* find node which contains requested index */
		for (int i = 0; i < timesOfJumps; ++i) {
			curr = curr.next;
			/* calculate index of requested item */
			pos -= maxArrayCapacity;
		}

		return new GITListIterator(pos, curr, index);
	}

	/**
	 * Creates List's structure by using E.toString() then returns it as a
	 * String.
	 * 
	 * @return Structure of List.
	 */
	@Override
	public String toString() {
		Node curr = head;
		StringBuilder sb = new StringBuilder("| ");
		while (true) {
			for (E ele : curr.elements)
				sb.append(ele).append(" | ");

			if (null == curr.next)
				break;

			sb.append("--\n--> | ");
			curr = curr.next;
		}
		return sb.toString();
	}

	/**
	 * Node of list.
	 * 
	 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
	 * 
	 */
	private class Node {

		/**
		 * Next node.
		 */
		private Node next;

		/**
		 * Previous node.
		 */
		private Node prev;

		/**
		 * The number of elements stored in this node.
		 */
		private int size;

		/**
		 * Array which stores elements.
		 */
		private E[] elements;

		/**
		 * Constructs a new node with size <tt>maxArrayCapacity</tt>.
		 */
		Node() {
			elements = (E[]) new Object[maxArrayCapacity];
			size = 0;
			next = null;
			prev = null;
		}

	}

	/* Node related helper methods */

	/**
	 * Inserts given element into given node. If node is full, creates a new
	 * node then moves half of current node's elements to new created node then
	 * inserts new node after the given node in linked list. After node
	 * operations, inserts given element into node which has least element. (one
	 * of current node or newly created node)
	 * 
	 * @param node
	 *            Node to insert.
	 * @param position
	 *            Position to insert.
	 * @param element
	 *            Element to insert.
	 */
	private void insertIntoNode(Node node, int position, E element) {
		// if node is full
		if (node.size == maxArrayCapacity) {

			Node newNode = new Node();
			int elementsTomove = (node.size / 2);
			int startIndex = (node.size - elementsTomove);
			int i = 0;

			for (i = 0; i < elementsTomove; ++i) {
				newNode.elements[i] = node.elements[startIndex + i];
				node.elements[startIndex + i] = null;
			}
			node.size -= elementsTomove;
			newNode.size = elementsTomove;

			newNode.next = node.next;
			newNode.prev = node;

			if (null != node.next) {
				node.next.prev = newNode;
			}

			node.next = newNode;

			if (tail == node) {
				tail = newNode;
			}

			if (position > node.size) {
				node = newNode;
				position -= newNode.size;
			}

		}

		// if there is enough space at node directly jump here

		// shift elements in node to open a cell for new element (if required)
		for (int i = node.size; i > position; --i) {
			node.elements[i] = node.elements[i - 1];
		}
		node.elements[position] = element;
		++(node.size);
		++size;

	}

	/**
	 * Merges next node into given node. Mostly useful when an item removed from
	 * list. Simply copies next node's element into current node's array then
	 * removes next node from list
	 * 
	 * @param toMerge
	 *            Node to merge into
	 */
	private void mergeWithNextNode(Node toMerge) {
		Node nextNode = toMerge.next;
		int currNodeSize = toMerge.size;
		int nextNodeSize = nextNode.size;

		for (int i = 0; i < nextNodeSize; ++i) {
			toMerge.elements[currNodeSize + i] = nextNode.elements[i];
			nextNode.elements[i] = null;
		}

		toMerge.size += nextNode.size;

		if (null != nextNode.next) {
			nextNode.next.prev = toMerge;
		}

		toMerge.next = nextNode.next.next;

		if (tail == nextNode) {
			tail = toMerge;
		}
		return;

	}

	/**
	 * Removes element at given position from given node. Never makes safety
	 * check. Simply shifts elements after given position through removed
	 * element. After remove operation, if current node can merge with next node
	 * or previous node, calls {@link GITList#mergeWithNextNode(Node)} to merge
	 * them
	 * 
	 * @param node
	 *            The node which contains element which marked for remove
	 * @param position
	 *            Position of element which marked for remove
	 */
	private void removeFromNode(Node node, int position) {
		--(node.size);
		for (int i = position; i < node.size; ++i) {
			node.elements[i] = node.elements[i + 1];
		}
		node.elements[node.size] = null;

		/* If there is a change to merge two nodes check for it */
		if (null != node.next) {
			/* here is a change to merge two nodes */
			if ((node.size + node.next.size) <= maxArrayCapacity) {
				mergeWithNextNode(node);
			}

		} else if (null != node.prev) {
			/* here is also a chance to merge two nodes */
			if ((node.size + node.prev.size) <= maxArrayCapacity) {
				mergeWithNextNode(node.prev);

			}

		}

		--size;
		return;
	}

	/**
	 * Iterator class of list. It is also used as Iterator since
	 * java.util.ListIterator extends java.util.Iterator
	 * 
	 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
	 * 
	 */
	private class GITListIterator implements ListIterator<E> {

		private int index; /* Index of list */
		private Node currNode; /* Current node which pointed by iterator */
		private int pos; /* Index of current Node */

		/**
		 * Default constructor for ListIterator. Sets iterator to point just
		 * before first element of first node
		 */
		public GITListIterator() {
			this.index = 0;
			this.currNode = head;
			this.pos = -1;
		}

		/**
		 * Constructor For GITListIterator class. Sets iterator to point node's
		 * posth element (and this is also indexth element of list)
		 * 
		 * @param node
		 *            Node to start iterator
		 * @param index
		 *            Index of List to start
		 * @param pos
		 *            Index of Node's array to Start
		 */
		private GITListIterator(int index, Node node, int pos) {
			this.index = index;
			this.currNode = node;
			this.pos = pos;
		}

		@Override
		public boolean hasNext() {
			return ((size - 1) > index);
		}

		/**
		 * If there in an element after position of iterator, returns it else
		 * throws exception
		 * 
		 * @return Element after position of iterator
		 * 
		 * @throws NoSuchElementException
		 *             If there is no element after position of iterator
		 */
		@Override
		public E next() {
			++pos;
			if (pos >= currNode.size) {
				if (null != currNode.next) {
					currNode = currNode.next;
					pos = 0;
				} else {
					throw new NoSuchElementException(
							"No more elements left at end of list.");
				}
			}
			++index;
			return currNode.elements[pos];
		}

		@Override
		public boolean hasPrevious() {
			return (0 < index);
		}

		/**
		 * If there in an element before position of iterator, returns it else
		 * throws exception
		 * 
		 * @return Element before position of iterator
		 * 
		 * @throws NoSuchElementException
		 *             If there is no element before position of iterator
		 */
		@Override
		public E previous() {
			--pos;
			if (0 > pos) {
				if (null != currNode.prev) {
					currNode = currNode.prev;
					pos = (currNode.size - 1);
				} else {
					throw new NoSuchElementException(
							"No more elements left at beginnig of list.");
				}
			}

			--index;
			return currNode.elements[pos];
		}

		@Override
		public int nextIndex() {
			return (index + 1);
		}

		@Override
		public int previousIndex() {
			return (index - 1);
		}

		/**
		 * Removes element at after current position of iterator. (in most cases
		 * same element with iterator.next() )
		 */
		@Override
		public void remove() {
			removeFromNode(currNode, pos);
		}

		@Override
		public void set(E e) {
			currNode.elements[pos] = e;

		}

		@Override
		public void add(E e) {
			insertIntoNode(currNode, pos, e);
		}

	}

} /* End of GITList<E> */
