import java.util.ListIterator;

import tr.edu.gyte.cse.cse222.makiftasova.hw03.GITList;

/**
 * CSE222 - HW03 - Main.java --  
 * 
 * Test Run of GITList<E> contains some ListIterator and basic add operations
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * 
 *         Student Number: 111044016
 */
public class Main {

	/**
	 * @param args
	 *            Arguments from Command Line (Has no use at this project)
	 */
	public static void main(String[] args) {

		GITList<Integer> a = new GITList<>();
		System.out.println("command: GITList<Integer> a = new GITList<>()\n");
		Integer i = 0;
		int cntr = 0;

		for (cntr = 0; cntr < 25; ++cntr) {
			System.out.println("command: a.addLast(" + i + ")");
			a.addLast(i++);
		}
		System.out.println();

		System.out.println("command: a.add(11, 99)");
		a.add(11, new Integer(99));

		System.out.println("command: a.add(5, 88)\n");
		a.add(5, new Integer(88));

		System.out.println("command: a.addFirst(77)");
		a.addFirst(77);

		System.out.println("command: a.addFirst(66)\n");
		a.addFirst(66);

		System.out.println("Elements in list:\n-----------------");
		cntr = 0;
		for (Integer ele : a) {
			System.out.print(ele + " ");
			cntr += 2;
			if (0 == (cntr % 40)) {
				System.out.println();
			}
		}
		System.out.println("\n");

		System.out.println("first item: " + a.getFirst());
		System.out.println("last item: " + a.getLast());
		int getPos = (a.size() / 2);
		System.out.printf("item at position %d: " + a.get(getPos), getPos);

		System.out.println("\n");

		System.out.println("Begin ListIterator test\n-----------------------");

		System.out.println();

		ListIterator<Integer> listIter = a.listIterator(5);

		System.out.println("listIter = a.listIterator(5);");

		System.out.println();

		System.out.println("List Structure:\n---------------");
		System.out.println(a);

		System.out.println("\nDoing some opeations...\n");

		System.out.println("listIter.add(999)");
		listIter.add(new Integer(999));
		System.out.println("listIter.add(888)");
		listIter.add(new Integer(888));
		System.out.println("listIter.add(777)");
		listIter.add(new Integer(777));

		System.out.println("listIter.next()");
		listIter.next();
		System.out.println("listIter.next()");
		listIter.next();
		System.out.println("listIter.next()");
		listIter.next();
		System.out.println("listIter.next()");
		listIter.next();

		System.out.println("listIter.add(666)");
		listIter.add(new Integer(666));
		System.out.println("listIter.add(555)");
		listIter.add(new Integer(555));

		System.out.println("listIter.next()");
		listIter.next();
		System.out.println("listIter.next()");
		listIter.next();
		System.out.println("listIter.next()");
		listIter.next();

		System.out.println("listIter.remove()");
		listIter.remove();
		System.out.println("listIter.remove()");
		listIter.remove();

		System.out.println("\nEnd of operations...\n");
		System.out.println("List Structure:\n---------------");
		System.out.println(a);

		System.out.println();

		System.out.println("listIter.hasNext(): " + listIter.hasNext());
		System.out.println("listIter.nextIndex(): " + listIter.nextIndex());
		System.out.println();
		System.out.println("listIter.hasPrevious(): " + listIter.hasPrevious());
		System.out.println("listIter.previousIndex(): "
				+ listIter.previousIndex());

		System.out.println("\n-----------------\n"
				+ "End of Sample Run\n-----------------");
	}

}
