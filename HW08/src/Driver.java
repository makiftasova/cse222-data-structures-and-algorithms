import java.util.Random;

//import com.makiftasova.assignments.cse222.hw08.AVLSearchTree;
import com.makiftasova.assignments.cse222.hw08.BinarySearchTree;

/**
 * CSE222 - HW08 - Driver -
 * 
 * Driver class of Project.
 * 
 * @author <a href="mailto:makiftasova@gmail.com">Mehmet Akif TAŞOVA</a> Student
 *         Number: 111044016
 * 
 * 
 */
public class Driver {

	public static final int MAX = 1600;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Random randGen = new Random();

//		AVLSearchTree<Integer> bst = new AVLSearchTree<>();
		BinarySearchTree<Integer> bst = new BinarySearchTree<>();

		int tmp = 0;

		for (int i = 0; i < MAX; ++i) {
			tmp = randGen.nextInt();
			bst.add(tmp);
		}

//		System.out.println("rotate: " + bst.numOfRots());
		
		System.out.println("insert: " + bst.numOfCompsInsert());

		bst.find(tmp);
		System.out.println("search: " + bst.numOfCompsSearch());

	}

}
