package com.makiftasova.assignments.cse222.hw08;

/**
 * CSE222 - HW08 - BinaryTree -
 * 
 * A basic Binary Tree interface.
 * 
 * @author <a href="mailto:makiftasova@gmail.com">Mehmet Akif TAŞOVA</a> Student
 *         Number: 111044016
 * 
 */
public interface BinaryTree<E> {

	/**
	 * Returns <tt>true</tt> if the tree is a leaf, <tt>false</tt> otherwise.
	 * 
	 * @return <tt>true</tt> if the tree is a leaf, <tt>false</tt> otherwise.
	 */
	public boolean isLeaf();

	/**
	 * Returns <tt>true</tt> if ((size() == 0) == true), <tt>false</tt>
	 * otherwise
	 * 
	 * @return <tt>true</tt> if tree is empty, otherwise <tt>false</tt>.
	 */
	public boolean isEmpty();

	/**
	 * Removes all the elements of tree.
	 */
	public void clear();

	/**
	 * Returns the number of elements in tree.
	 * 
	 * @return number of elements
	 */
	public int size();

}
