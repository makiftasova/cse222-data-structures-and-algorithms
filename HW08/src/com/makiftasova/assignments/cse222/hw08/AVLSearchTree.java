package com.makiftasova.assignments.cse222.hw08;

import java.io.Serializable;

/**
 * CSE222 - HW08 - AVLSearchTree -
 * 
 * An AVL Search Tree implementation based on {@link SearchTree} and
 * {@link BinaryTree}.
 * 
 * @author <a href="mailto:makiftasova@gmail.com">Mehmet Akif TAŞOVA</a> Student
 *         Number: 111044016
 * 
 */
public class AVLSearchTree<E extends Comparable<E>> implements Serializable,
		SearchTree<E>, BinaryTree<E> {

	/**
	 * Serializable Version ID
	 */
	private static final long serialVersionUID = -2998374847036892121L;

	/**
	 * Default capacity value of AVLSearchTree
	 */
	private static final int DEFAULT_CAPACITY = 16;

	/**
	 * Choice of method to clean. if <tt>true</tt> simply creates a new array
	 * for data, if <tt>false</tt> assigns <tt>null</tt> to every non-null node.
	 * Can only modify before compile time.
	 */
	private static final boolean fast_clear = true;

	/**
	 * Not found flag for index finder methods
	 */
	public static final int NOT_FOUND = -1;

	/**
	 * Array to store tree
	 */
	private E[] data;

	/**
	 * Stores balance status of corresponding node at {@link AVLSearchTree#data}
	 */
	private int[] balanceData;

	/**
	 * Current capacity of array
	 */
	private int capacity;

	/**
	 * Current number of elements in tree
	 */
	private int size; // current size of array

	/**
	 * For Test Purpose
	 */
	private int numOfRotate = 0;

	/**
	 * For Search Comparison test purposes
	 */
	private int numOfCompsSearch = 0;

	/**
	 * For Insert Comparison test purposes
	 */
	private int numOfCompsIns = 0;

	/**
	 * Creates a Binary Search Tree with capacity of 16
	 */
	@SuppressWarnings("unchecked")
	public AVLSearchTree() {
		capacity = DEFAULT_CAPACITY;
		size = 0;
		data = (E[]) new Comparable[capacity];
		balanceData = new int[capacity];
	}

	/**
	 * Creates a Binary Search Tree with capacity of initialCapacity. If
	 * (initialCapacity < 3) if true, initializes Binary Search Tree with
	 * capacity of 16
	 * 
	 * @param initialCapacity
	 *            Initial capacity of Tree
	 */
	@SuppressWarnings("unchecked")
	public AVLSearchTree(int initialCapacity) {
		// If initial capacity is illegal, then make it legal :)
		initialCapacity = ((initialCapacity < 3) ? DEFAULT_CAPACITY
				: initialCapacity);

		// Now prepare object safely
		capacity = initialCapacity;
		size = 0;
		data = (E[]) new Comparable[capacity];
		balanceData = new int[capacity];
	}

	@Override
	public boolean add(E item) {

		// if item is already in tree
		if (this.contains(item)) {
			return false;
		}

		int currIndex = 0;

		while (true) {

			if (data[currIndex] == null) {
				++numOfCompsIns; // For Test Purposes
				data[currIndex] = item;
				balanceData[currIndex] = 0;
				int indexParent = findParentIndex(currIndex);
				while (indexParent > 0) {
					balanceData[indexParent] = calculateBalance(indexParent);
					indexParent = findParentIndex(indexParent);
				}
				balanceData[0] = calculateBalance(0);
				++size;
				balance(indexParent);
				return true;
			}
			if (item.compareTo(data[currIndex]) == 0) {
				// if item == data[index]
				// item is equal to current node's data
				// this means item is already in search tree
				return false;
			} else if (item.compareTo(data[currIndex]) > 0) {
				// item > data[currIndex]
				currIndex = getRigthIndex(currIndex);
				while (capacity <= currIndex) {
					reallocate();
				}

			} else if (item.compareTo(data[currIndex]) < 0) {
				// item < data[currIndex]
				currIndex = getLeftIndex(currIndex);
				while (capacity <= currIndex) {
					reallocate();
				}
			}
			++numOfCompsIns; // For Test Purposes
		}
	}

	@Override
	public boolean contains(E target) {
		if (null == target) {
			return false;
		}
		return (null != this.find(target));
	}

	@Override
	public E find(E target) {

		if (null == target) {
			return null;
		}

		int index = findIndex(target);
		E foundItem = ((index != NOT_FOUND) ? data[index] : null);
		return foundItem;
	}

	@Override
	public E delete(E target) {

		int index = findIndex(target);

		if (NOT_FOUND == index) { // target not found
			return null;
		}

		--size; // decrease size
		if (isLeaf(index)) { // node to delete has no child
			data[index] = null;
			balanceData[index] = 0;
		} else if (null == data[getLeftIndex(index)]) {
			// node to delete has only right child
			shiftRightToParent(getRigthIndex(index));
		} else { // node to delete has left child
			shiftAndDelete(target);
		}

		balance(findParentIndex(index));

		return target;
	}

	@Override
	public boolean remove(E target) {
		return (null != this.delete(target));
	}

	@Override
	public int maxDepth() {
		return findDepthOfTree(0);
	}

	@Override
	public boolean isLeaf() {
		return isLeaf(0);
	}

	@Override
	public boolean isEmpty() {
		return (this.size() == 0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void clear() {
		if (fast_clear) {
			data = (E[]) new Comparable[capacity];
			size = 0;
		} else {
			clearChilds(0);
		}
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		printToSb(sb, 0);
		return sb.toString();
	}

	/**
	 * Wrapper function for
	 * {@link AVLSearchTree#printToSb(StringBuilder, String, int)}
	 * 
	 * @param sb
	 *            String Builder.
	 * @param index
	 *            Index to begin
	 */
	private void printToSb(StringBuilder sb, int index) {
		printToSb(sb, "", index);
	}

	/**
	 * A Helper function for toString method
	 * 
	 * @param sb
	 *            String Builder from toString
	 * @param prefix
	 *            Some visual thing
	 * @param index
	 *            Begin Index of tree
	 */
	private void printToSb(StringBuilder sb, String prefix, int index) {
		if (index >= capacity) {
			return;
		}

		if (index == 0) {
			sb.append(data[index]).append("(b: " + balanceData[index] + ")")
					.append('\n');
		} else {

			sb.append(data[index]).append("(b: " + balanceData[index] + ")")
					.append('\n');
			prefix = prefix + "|   ";
		}

		if (getLeftIndex(index) < capacity) {
			sb.append(prefix);
			sb.append("|---");
			printToSb(sb, prefix, getLeftIndex(index));
		}

		if (getRigthIndex(index) < capacity) {
			sb.append(prefix);
			sb.append("|---");
			printToSb(sb, prefix, getRigthIndex(index));
		}
	}

	/**
	 * Finds depth of given tree
	 * 
	 * @param index
	 *            Index of root of tree
	 * @return Depth of tree
	 */
	private int findDepthOfTree(int index) {
		if ((index >= capacity) || (null == data[index])) {
			return 0;
		} else {
			return (Math.max(findDepthOfTree(getLeftIndex(index)),
					findDepthOfTree(getRigthIndex(index))) + 1);
		}
	}

	/**
	 * Finds given items index in array
	 * 
	 * @param item
	 *            Item to find index
	 * @return Index of item if exits. Returns {@link AVLSearchTree#NOT_FOUND}
	 *         if item not found.
	 */
	private int findIndex(E item) {
		int index = 0;

		if (null == item) {
			++numOfCompsSearch; // For Test Purposes
			return NOT_FOUND;
		}

		if (this.size() == 0) {
			++numOfCompsSearch; // For Test Purposes
			return NOT_FOUND;
		}

		while (true) {
			if (index >= capacity) {
				break;
			} else if (null == data[index]) {
				break;
			} else if (item.compareTo(data[index]) == 0) {
				// if item == data[index]
				return index;
			} else if (item.compareTo(data[index]) > 0) {
				// item > data[index]
				index = getRigthIndex(index);
			} else if (item.compareTo(data[index]) < 0) {
				// item < data[index]
				index = getLeftIndex(index);
			}
			++numOfCompsSearch; // For Test Purposes
		}
		return NOT_FOUND;
	}

	/**
	 * Shifts required sub nodes and deletes given target from tree
	 * 
	 * @param target
	 *            Target to delete.
	 * @return Deleted target. If given target is not in tree, returns
	 *         <tt>null</tt>.
	 */
	private E shiftAndDelete(E target) {

		int index = findIndex(target);
		if (index < capacity && index != -1) {
			data[index] = shiftAndDelete(getMaxItem(index));

			if (null == data[index]) {
				balanceData[index] = 0;
			} else {
				balanceData[index] = balanceData[getIndexOfMax(index)];
			}

			// balanceData[index] =
			// ((data[index] == null) ? 0
			// : balanceData[getIndexOfMax(index)]);

			return target;
		}
		return null;
	}

	/**
	 * Returns index of right child of element at given index
	 * 
	 * @param index
	 *            Index of element to find right child'sindex
	 * @return Index of right child
	 */
	private int getRigthIndex(int index) {
		return ((2 * index) + 2);
	}

	/**
	 * Returns index of left child of element at given index
	 * 
	 * @param index
	 *            Index of element to find left child'sindex
	 * @return Index of left child
	 */
	private int getLeftIndex(int index) {
		return ((2 * index) + 1);
	}

	/**
	 * Returns maximum from sub nodes of given node.
	 * 
	 * @param index
	 *            Node to begin search.
	 * @return {@link AVLSearchTree#NOT_FOUND} if given node is null, else index
	 *         of MaxItem.
	 */
	private int getIndexOfMax(int index) {
		int maxIndex = index;

		if (capacity <= index || data[index] == null)
			return index;

		maxIndex = getIndexOfMax(getRigthIndex(maxIndex));

		if (capacity <= maxIndex
				|| data[maxIndex] == null
				|| (data[maxIndex].compareTo(data[index]) <= 0))
			return index;
		return maxIndex;
	}

	/**
	 * Returns Maximum item in given sub-tree
	 * 
	 * @param index
	 *            Index to begin search
	 * 
	 * @return <tt>null</tt> if item not found, found item otherwise.
	 */
	private E getMaxItem(int index) {
		int indexOfMax = getIndexOfMax(getLeftIndex(index));
		if (index < capacity && indexOfMax < capacity)
			return data[indexOfMax];
		return null;
	}

	/**
	 * Finds Parent index of given node
	 * 
	 * @param childIndex
	 *            Index of child node.
	 * @return Index of Parent Node.
	 */
	private int findParentIndex(int childIndex) {
		return ((childIndex - 1) / 2);
	}

	/**
	 * Shifts right side of tree to parent of tree.
	 * 
	 * @param index
	 *            Root index of tree.
	 */
	private void shiftRightToParent(int index) {

		if ((index >= capacity) || (null == data[index])) {
			return;
		}

		int newIndex = findParentIndex(index);
		int RIndex = getRigthIndex(index);
		int LIndex = getLeftIndex(index);

		data[newIndex] = data[index];
		balanceData[newIndex] = balanceData[index];

		if (RIndex < capacity) {
			data[getRigthIndex(newIndex)] = data[RIndex];
			balanceData[getRigthIndex(newIndex)] = balanceData[RIndex];
		} else {
			data[getRigthIndex(newIndex)] = null;
			balanceData[getRigthIndex(newIndex)] = 0;
		}

		if (LIndex < capacity) {
			data[getLeftIndex(newIndex)] = data[LIndex];
			balanceData[getLeftIndex(newIndex)] = balanceData[LIndex];
		} else {
			data[getLeftIndex(newIndex)] = null;
			balanceData[getLeftIndex(newIndex)] = 0;
		}

		shiftRightToParent(getRigthIndex(index));
		shiftRightToParent(getLeftIndex(index));
	}

	/**
	 * Checks sub-tree ad index, if it is a leaf tree, returns <tt>true</tt>,
	 * <tt>false</tt> otherwise.
	 * 
	 * @param index
	 *            Index of subtree
	 * @return If sub-tree is a leaf tree, returns <tt>true</tt>, <tt>false</tt>
	 *         otherwise.
	 */
	private boolean isLeaf(int index) {

		if (getLeftIndex(index) >= capacity) {
			return true;
		}

		return ((data[getRigthIndex(index)] == null)
		&& (data[getLeftIndex(index)] == null));
	}

	/**
	 * Clears given tree's all nodes and children.
	 * 
	 * @param index
	 *            Root index of tree.
	 */
	private void clearChilds(int index) {
		if (index >= capacity) {
			return;
		}
		if (data[index] == null) {
			return;
		}

		clearChilds(getRigthIndex(index));
		clearChilds(getLeftIndex(index));
		data[index] = null;
		size = 0;
	}

	/**
	 * Increases size of array to <code>(capacity * 2)</code>
	 */
	@SuppressWarnings("unchecked")
	private void reallocate() {
		capacity = (capacity * 2);

		E[] tmpData = (E[]) new Comparable[capacity];
		int[] tmpBal = new int[capacity];

		System.arraycopy(data, 0, tmpData, 0, capacity / 2);
		System.arraycopy(balanceData, 0, tmpBal, 0, capacity / 2);

		data = tmpData;
		balanceData = tmpBal;
	}

	/**
	 * Returns balance of given node. If given index is out of bounds, returns
	 * {@link AVLSearchTree#NOT_FOUND}
	 * 
	 * @param index
	 *            Index of node.
	 * @return Balance of node. 0, if given index is out of bounds.
	 * 
	 */
	private int getBalanceOf(int index) {
		try {
			return balanceData[index];
		} catch (ArrayIndexOutOfBoundsException e) {
			return 0;
		}
	}

	/**
	 * 
	 * Calculates balance of given node.
	 * 
	 * @param index
	 *            Index of node.
	 * @return Balance value of node.
	 */
	private int calculateBalance(int index) {

		if (getRigthIndex(index) >= capacity) {
			return 0;
		}
		return (findDepthOfTree(getRigthIndex(index)) - findDepthOfTree(getLeftIndex(index)));
	}

	/**
	 * Balances given node if required.
	 * 
	 * @param index
	 *            Index of node.
	 */
	private void balance(int index) {
		int balanceVal = getBalanceOf(index);
		if (balanceVal < -1) {
			// right rotate
			++numOfRotate; // For Test Purpose
			rightRotate(index);
		} else if (balanceVal > 1) {
			// left rotate
			leftRotate(index);
			++numOfRotate; // For Test Purpose
		} else {
			return; // nothing to do
		}
		return;
	}

	/**
	 * Counting Number Of Rotations for Testing.
	 * 
	 * @return Number of Rotations.
	 */
	public int numOfRots() {
		return numOfRotate;
	}

	/**
	 * For Search Comparison test purposes.
	 * 
	 * @return Number of comparisons for Inserting.
	 */
	public int numOfCompsInsert() {
		return numOfCompsIns;
	}

	/**
	 * For Insert Comparison test purposes
	 * 
	 * @return Number of comparisons for Searching.
	 */
	public int numOfCompsSearch() {
		return numOfCompsSearch;
	}

	/**
	 * Rotates given node to right.
	 * 
	 * @param index
	 *            Index of node to rotate.
	 */
	private void rightRotate(int index) {
		E tmp = data[index];
		pushUp(getLeftIndex(index));
		data[getRigthIndex(index)] = tmp;
		return;
	}

	/**
	 * Rotates given node to left.
	 * 
	 * @param index
	 *            Index of node to rotate.
	 */
	private void leftRotate(int index) {
		E tmp = data[index];
		pushUp(getRigthIndex(index));
		data[getLeftIndex(index)] = tmp;
		return;
	}

	/**
	 * Moves given tree to given index. If <code>newIndex <= rootIndex </code>,
	 * does nothing to tree.
	 * 
	 * @param rootIndex
	 *            Current Root index of tree.
	 * @param newIndex
	 *            New root index of tree.
	 */
	private void moveTreeDown(int rootIndex, int newIndex) {

		// if (newIndex <= rootIndex) {
		// return;
		// }

		if (null != data[rootIndex]) {

			while (capacity <= newIndex) {
				reallocate();
			}
			moveTreeDown(getLeftIndex(rootIndex), getLeftIndex(newIndex));
			moveTreeDown(getRigthIndex(rootIndex), getRigthIndex(newIndex));

			data[newIndex] = data[rootIndex];
			data[rootIndex] = null;
			balanceData[newIndex] = balanceData[rootIndex];
			balanceData[rootIndex] = 0;
		}

	}

	/**
	 * Assigns <code>null</code> to all elements of given tree.
	 * 
	 * @param index
	 *            Root index of tree.
	 */
	private void clearSubTree(int index) {
		if ((index >= capacity) || (index < 0)) {
			return;
		}
		data[index] = null;
		balanceData[index] = 0;
		clearSubTree(getRigthIndex(index));
		clearSubTree(getLeftIndex(index));
		int parentIndex = findParentIndex(index);
		balanceData[parentIndex] = calculateBalance(parentIndex);
	}

	/**
	 * Pushed given node to one level up.
	 * 
	 * @param index
	 *            Index of node.
	 */
	private void pushUp(int index) {
		int indexParent = findParentIndex(index);
		if (capacity <= index) {
			return;
		}
		data[indexParent] = data[index];
		data[index] = null;

		data[getLeftIndex(indexParent)] = data[getLeftIndex(index)];
		data[getLeftIndex(index)] = null;

		data[getRigthIndex(indexParent)] = data[getRigthIndex(index)];
		data[getRigthIndex(index)] = null;

		if (null != data[getLeftIndex(index)]) {
			pushUp(getLeftIndex(index));
		}

		if (null != data[getRigthIndex(index)]) {
			pushUp(getRigthIndex(index));
		}
	}
}
