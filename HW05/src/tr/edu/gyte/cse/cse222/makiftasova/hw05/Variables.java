package tr.edu.gyte.cse.cse222.makiftasova.hw05;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * CSE222 - HW04 - GITLanguage - Variable List class. Uses a LinkedList for
 * storing variables.
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public class Variables implements Iterable<Variable> {

	/**
	 * Sign of variable not found in list
	 */
	public static final int NOT_FOUND = -1;

	/**
	 * Real variable list
	 */
	private LinkedList<Variable> m_variableList;

	/**
	 * Creates an empty variables list.
	 */
	public Variables() {
		m_variableList = new LinkedList<>();
	}

	/**
	 * Adds given variable into Variables list. If another variable with same
	 * name exists, returns false and will not adds new variable to list.
	 * 
	 * @param var
	 *            Variable to add.
	 * @return If given variable is not in list returns <tt>true</tt>, else
	 *         returns <tt>false</tt>.
	 */
	public boolean add(Variable var) {
		if (var == null) {
			return false;
		}
		if (!(this.hasVariable(var.getName()))) {
			m_variableList.add(var);
			return true;
		}
		return false;
	}

	/**
	 * Checks is there a variable named <tt>name</tt>. If there is a variable
	 * exists with <tt>name</tt> returns <tt>true</tt>, else returns
	 * <tt>false</tt>.
	 * 
	 * @param name
	 *            Name of variable to search.
	 * @return <tt>true</tt> if there is a variable named with given
	 *         <tt>name</tt>. Else returns <tt>false</tt>.
	 */
	public boolean hasVariable(String name) {
		for (Variable v : m_variableList) {
			if (name.equals(v.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Search list for given Variable <tt>var</tt> and returns <tt>true</tt> if
	 * it is in the list, else returns <tt>false</tt>.
	 * 
	 * @param var
	 *            Variable to search.
	 * @return <tt>true</tt> if given variable is in the list, else returns
	 *         <tt>false</tt>.
	 */
	public boolean hasVariable(Variable var) {
		return m_variableList.contains(var);
	}

	/**
	 * Searches given variable name in variables list. If variable is not found,
	 * returns <tt>NOT_FOUND</tt> as index
	 * 
	 * @param name
	 *            Variable name to search
	 * @return Index of variable. If variable is not found, returns
	 *         <tt>NOT_FOUND</tt>
	 */
	public int indexOf(String name) {
		if (name == null) {
			return NOT_FOUND;
		}

		int index = NOT_FOUND;
		for (Variable v : m_variableList) {
			if (name.equals(v.getName())) {
				index = m_variableList.indexOf(v);
			}
		}
		return index;
	}

	/**
	 * Returns index of given variable if it is in variable list
	 * 
	 * @param var
	 *            Variable to search
	 * @return Index of given variable if it is in variable list.
	 */
	public int indexOf(Variable var) {
		if (var == null) {
			return NOT_FOUND;
		}

		if (m_variableList.contains(var)) {
			return m_variableList.indexOf(var);
		}
		return NOT_FOUND;
	}

	/**
	 * Returns variable at given index. If index out of bounds, returns
	 * <tt>null</tt>.
	 * 
	 * @param index
	 *            Index of variable
	 * @return Returns Variable at given index. If index out of bounds, returns
	 *         <tt>null</tt>.
	 */
	public Variable get(int index) {
		try {
			return m_variableList.get(index);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	/**
	 * Returns Variable named <tt>name</tt> in list. If there is no variable
	 * named <tt>name</tt>, return <tt>null</tt>.
	 * 
	 * @param name
	 *            Name of variable to get.
	 * @return Variable named <tt>name</tt> in list. If there is no variable
	 *         named <tt>name</tt>, return <tt>null</tt>.
	 */
	public Variable get(String name) {
		return this.get(this.indexOf(name));
	}

	/**
	 * Removes Variable at given index and returns it when removing. If given
	 * variable is not in list, return <tt>null</tt>.
	 * 
	 * @param index
	 *            Index of variable to remove.
	 * @return Removed variable from list. If given variable is not in list,
	 *         returns <tt>null</tt>.
	 */
	public Variable remove(int index) {
		try {
			return m_variableList.remove(index);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	/**
	 * Removes given variable with <tt>name</tt> from Variables list then
	 * returns it. If there is no variable named <tt>name</tt> returns
	 * <tt>null</tt>.
	 * 
	 * @param name
	 *            Name of variable to remove.
	 * @return Returns removed variable. If there is no variable named
	 *         <tt>name</tt> returns <tt>null</tt>.
	 */
	public Variable remove(String name) {
		return this.remove(this.indexOf(name));
	}

	/**
	 * Removes given variable from list hen returns it. If given variable is not
	 * in list, return <tt>null</tt>.
	 * 
	 * @param var
	 *            Variable to remove from list
	 * @return Removed variable from list. If given variable is not in list,
	 *         returns <tt>null</tt>.
	 */
	public Variable remove(Variable var) {
		return this.remove(m_variableList.indexOf(var));
	}

	/**
	 * Returns an Iterator for variables list
	 * 
	 * @return An Iterator for variables list
	 */
	@Override
	public Iterator<Variable> iterator() {
		return m_variableList.iterator();
	}

}
