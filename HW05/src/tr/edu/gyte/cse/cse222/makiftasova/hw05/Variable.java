package tr.edu.gyte.cse.cse222.makiftasova.hw05;

/**
 * 
 * CSE222 - HW04 - GITLanguage - Variable class for type, name and value of
 * variables. Each object of this class can store a varaible of type int or
 * float.
 * 
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public class Variable {

	private int m_type;
	private String m_name;
	private Number m_value;

	/**
	 * Constructor of Variable class. creates a new variable with given details.
	 * 
	 * @param type
	 *            Type code of variable.
	 * @param name
	 *            Name of variable.
	 * @param value
	 *            Value of variable. only {@link Integer} or {@link Float}.
	 *            accepted
	 * @throws UnknownDataTypeException
	 *             If given type code is unknown.
	 * @throws ClassCastException
	 *             If given value is neither {@link Integer} nor {@link Float}.
	 */
	public Variable(int type, String name, Number value)
			throws DataTypeException, ClassCastException {

		if ((type < 1) || (type > 2)) {
			throw new DataTypeException(type);
		}

		if ((value instanceof Integer) && type == Type.INT) {
			m_value = value;
		} else if ((value instanceof Float) && (type == Type.FLOAT)) {
			m_value = value;
		} else {
			throw new ClassCastException(value.getClass().getName()
					+ " is not acceptable as type of value");
		}

		m_type = type;
		m_name = name;

	}

	/**
	 * Changes current value of variable with given one. Only accepts
	 * {@link Integer} or {@link Float} references. If there is another type of
	 * reference is given, throws {@link ClassCastException}.
	 * 
	 * @param value
	 *            New value of variable
	 * @throws ClassCastException
	 *             If given reference is not a {@link Integer} or {@link Float}
	 */
	public void setValue(Number value) throws ClassCastException {
		if ((value instanceof Integer) || (value instanceof Float)) {
			m_value = value;
			return;
		}

		throw new ClassCastException(value.getClass().getName()
				+ " is not acceptable as type of value");
	}

	/**
	 * Returns Type code of variable. type codes are also defined as public
	 * static final int at this class
	 * 
	 * @return Type code of variable
	 */
	public int getType() {
		return m_type;
	}

	/**
	 * Returns name of variable
	 * 
	 * @return Name of variable
	 */
	public String getName() {
		return m_name;
	}

	/**
	 * Returns value of variable. Type of value could be {@link Integer} or
	 * {@link Float} depended on type of variable.
	 * 
	 * @return Value of variable
	 */
	public Number getValue() {
		return m_value;
	}

	/**
	 * Returns value of an int variable. If variable is not an int throws
	 * <tt>UnsupportedOperationException</tt>.
	 * 
	 * @return Value of an int variable.
	 * @throws UnsupportedOperationException
	 *             If variable is not an int.
	 */
	public int toInt() throws UnsupportedOperationException {
		if (m_value instanceof Integer)
			return ((Integer) m_value).intValue();

		throw new UnsupportedOperationException("Variable \"" + m_name
				+ "\" is not an int.");
	}

	/**
	 * Returns value of a float variable. If variable is not a float throws
	 * <tt>UnsupportedOperationException</tt>.
	 * 
	 * @return Value of a float variable.
	 * @throws UnsupportedOperationException
	 *             If variable is not a float.
	 */
	public float toFloat() throws UnsupportedOperationException {
		if (m_value instanceof Float)
			return ((Float) m_value).floatValue();

		throw new UnsupportedOperationException("Variable \"" + m_name
				+ "\" is not a float.");
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append((m_type == Type.INT) ? Keyword.INT : Keyword.FLOAT);
		s.append(" ").append(m_name).append(" = ").append(m_value);
		return s.toString();
	}

	/**
	 * Exception class for Unknown data types. all type codes are assumed
	 * unknown except <tt>TYPE_INT</tt> and <tt>TYPE_FLOAT</tt> *
	 */
	public static final class DataTypeException extends Exception {

		private static final long serialVersionUID = 3946500334000121454L;

		public DataTypeException(int typeCode) {
			super("Unknown type code: " + typeCode);
		}
	}
}
