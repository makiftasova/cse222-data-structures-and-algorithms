package tr.edu.gyte.cse.cse222.makiftasova.hw05;

import java.util.*;

/**
 * 
 * CSE222 - HW04 - GITLanguage - Class that can evaluate a postfix expression.
 * This class is a modified version of Koffman & Wolfgang's
 * {@link IntegerPostfixEvaluator} class for working on float type. Operator '^'
 * suppoert of this class added by Mehmet Akif TAŞOVA.
 * 
 * Student: Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 * @author Koffman & Wolfgang
 * 
 * */

public class FloatPostfixEvaluator {

	/**
	 * Error sting for Stack Empty error
	 */
	public static final String STACK_EMPTY_ERR = "The stack is empty";

	/**
	 * Error string for Stack is not Empty error
	 */
	public static final String STACK_NOT_EMPTY_ERR = "Stack should be empty";

	/**
	 * Error string gor stack is not empty error
	 */
	public static final String INVALID_CHAR_ERR = "Invalid character encountered: ";

	// Nested Class
	/**
	 * Class to report a syntax error when evaluating a postfix expression.
	 */
	public static class FloatEvalErrorException extends Exception {

		private static final long serialVersionUID = 8802428693135337803L;

		/**
		 * Construct a SyntaxError with the specified message.
		 * 
		 * @param message
		 *            The message
		 */
		FloatEvalErrorException(String message) {
			super(message);
		}
	}

	// Constant
	/** A list of operators. */
	private static final String OPERATORS = "+-*/^";

	// Data Field
	/** The operand stack. */
	private Stack<Float> operandStack;

	// Methods
	/**
	 * Evaluates the current operation. This function pops the two operands off
	 * the operand stack and applies the operator.
	 * 
	 * @param op
	 *            A character representing the operator
	 * @return The result of applying the operator
	 * @throws EmptyStackException
	 *             if pop is attempted on an empty stack
	 */
	private float evalOp(char op) {
		// Pop the two operands off the stack.
		float rhs = operandStack.pop();
		float lhs = operandStack.pop();
		float result = 0;
		// Evaluate the operator.
		switch (op) {
		case '+':
			result = lhs + rhs;
			break;
		case '-':
			result = lhs - rhs;
			break;
		case '/':
			result = lhs / rhs;
			break;
		case '*':
			result = lhs * rhs;
			break;
		case '^':
			result = (float) Math.pow(lhs, rhs);
			break;

		}
		return result;
	}

	/**
	 * Determines whether a character is an operator.
	 * 
	 * @param ch
	 *            The character to be tested
	 * @return true if the character is an operator
	 */
	private boolean isOperator(char ch) {
		return OPERATORS.indexOf(ch) != -1;
	}

	/**
	 * Evaluates a postfix expression.
	 * 
	 * @param expression
	 *            The expression to be evaluated
	 * @return The value of the expression
	 * @throws SyntaxError
	 *             if a syntax error is detected
	 */
	public float eval(String expression) throws FloatEvalErrorException {
		// Create an empty stack.
		operandStack = new Stack<Float>();

		// Process each token.
		StringTokenizer tokens = new StringTokenizer(expression);
		try {
			while (tokens.hasMoreTokens()) {
				String nextToken = tokens.nextToken();
				// Does it start with a digit?
				if (Character.isDigit(nextToken.charAt(0))) {
					// Get the integer value.
					float value = Float.parseFloat(nextToken);
					// Push value onto operand stack.
					operandStack.push(value);
				} // Is it an operator?
				else if (isOperator(nextToken.charAt(0))) {
					// Evaluate the operator.
					float result = evalOp(nextToken.charAt(0));
					// Push result onto the operand stack.
					operandStack.push(result);
				} else {
					// Invalid character.
					throw new FloatEvalErrorException(INVALID_CHAR_ERR
							+ nextToken);
				}
			} // End while.

			// No more tokens - pop result from operand stack.
			float answer = operandStack.pop();
			// Operand stack should be empty.
			if (operandStack.empty()) {
				return answer;
			} else {
				// Indicate syntax error.
				throw new FloatEvalErrorException(STACK_NOT_EMPTY_ERR);
			}
		} catch (EmptyStackException ex) {
			// Pop was attempted on an empty stack.
			throw new FloatEvalErrorException(STACK_EMPTY_ERR);
		}
	}
}
