package tr.edu.gyte.cse.cse222.makiftasova.hw05;

/**
 * 
 * CSE222 - HW04 - GITLanguage - Accepted keywords by GITlanguage.
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public interface Keyword {
	public static final String INT = "int";
	public static final String FLOAT = "float";
	public static final String PRINT = "print";
	public static final String LOOP = "loop";
	public static final String BEGIN = "begin";
	public static final String END = "end";
}
