package tr.edu.gyte.cse.cse222.makiftasova.hw05;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Scanner;

/**
 * 
 * CSE222 - HW04 - GITLanguage - 
 * 
 * FileReader Class, reads every line in given source file into a list of lines.
 * 
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public class FileReader implements Iterable<String> {

	private Scanner m_fileScanner;
	private File m_sourceFile; // source file
	private ArrayList<String> m_lines; // Lines of source file

	public static final String FILE_EXTENSION = "git";

	/**
	 * Constructor of class. reads every line from given file into list.
	 * 
	 * @param sourceFile
	 *            The file which contains source code
	 */
	public FileReader(String sourceFile) {
		m_lines = new ArrayList<>();
		String[] fileName;
		fileName = sourceFile.split("\\.");

		if (fileName[fileName.length - 1].equals(FILE_EXTENSION)) {
			m_sourceFile = new File(sourceFile);

			try {
				m_fileScanner = new Scanner(m_sourceFile);
				readFile();
			} catch (FileNotFoundException e) {
				System.out.printf("ERROR: File Not Found: %s\n", sourceFile);
				System.exit(ExitCode.FILE_NOT_FOUND);
			}
		} else {
			System.out.print("ERROR: Invalid file extension. ");
			System.out.println("Extension must be \".git\"");
			System.exit(ExitCode.INVALID_FILE_EXTENSION);
		}

	}

	/**
	 * Returns number of lines of source file
	 * 
	 * @return Number of lines of source file
	 */
	public int numberOfLines() {
		return m_lines.size();
	}

	/**
	 * Returns list iterator of inner list
	 * 
	 * @return List iterator of inner list
	 */
	public ListIterator<String> getListIterator() {
		return m_lines.listIterator();
	}

	/**
	 * Returns iterator of inner list
	 * 
	 * @return Iterator of inner list
	 */
	public Iterator<String> iterator() {
		return m_lines.listIterator();
	}

	/**
	 * Reads every sinlge line of given source file into ArrayList
	 * <tt>m_lines</tt>
	 */
	private void readFile() {
		while (m_fileScanner.hasNextLine()) {
			m_lines.add(m_fileScanner.nextLine());
		}
	}
}
