package tr.edu.gyte.cse.cse222.makiftasova.hw05;

import tr.edu.gyte.cse.cse222.makiftasova.hw05.GITInterpreter.MixedTypeException;
import tr.edu.gyte.cse.cse222.makiftasova.hw05.GITInterpreter.SyntaxErrorException;
import tr.edu.gyte.cse.cse222.makiftasova.hw05.Type.UnknownTypeException;

/**
 * 
 * CSE222 - HW04 - GITLanguage - Main class for classes of GITLanguage project.
 * Takes source file name from user and sends it into interpreter.
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public class Main {

	/**
	 * 
	 * One Function to rule them all.
	 * 
	 * @param args
	 *            Arguments from CLI, given by user.
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			if (args.length < 1) {
				System.out.println("ERROR: No source file given!\n");
			} else {
				System.out.println("ERROR: Too many arguments!\n");
			}
			System.out.print("USAGE:\n------\n");
			System.out.println("Gitlanguage [SOURCE FILE]\n");
			System.out.println("SOURCE FILE extension must be \".git\"");
			System.exit(ExitCode.USAGE_PRINT);
		}

		GITInterpreter interpreter = new GITInterpreter(args[0]);

		try {
			interpreter.eval();
		} catch (SyntaxErrorException | UnknownTypeException
				| MixedTypeException e) {
			System.out.println(e.getMessage());
		}

	}

}
