package tr.edu.gyte.cse.cse222.makiftasova.hw05;

/**
 * 
 * CSE222 - HW04 - GITLanguage -
 * 
 * Contains Exits codes for Exits with failure.
 * 
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public interface ExitCode {

	/**
	 * Exit code for File Not Found error.
	 */
	public static final int FILE_NOT_FOUND = 1;

	/**
	 * Exit code for Invalid File Extension error.
	 */
	public static final int INVALID_FILE_EXTENSION = 2;

	/**
	 * Exit code for exiting because of calling interpreter with illegal
	 * arguments.
	 */
	public static final int USAGE_PRINT = 3;

	/**
	 * Exit code for exiting after any kind of unknows errors.
	 */
	public static final int UNKNOWN_ERROR = 4;

}
