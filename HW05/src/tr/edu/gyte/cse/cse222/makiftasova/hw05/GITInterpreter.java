package tr.edu.gyte.cse.cse222.makiftasova.hw05;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Stack;
import java.util.StringTokenizer;

import tr.edu.gyte.cse.cse222.makiftasova.hw05.FloatPostfixEvaluator.FloatEvalErrorException;

import tr.edu.gyte.cse.cse222.makiftasova.hw05.InfixToPostfix.ConvertErrorException;

import tr.edu.gyte.cse.cse222.makiftasova.hw05.IntegerPostfixEvaluator.EvalErrorException;

import tr.edu.gyte.cse.cse222.makiftasova.hw05.Type.UnknownTypeException;
import tr.edu.gyte.cse.cse222.makiftasova.hw05.Variable.DataTypeException;

/**
 * CSE222 - HW04 - GITLanguage - Interpreter class. Reads all commands from
 * source file, then interprets it. When a new varaible declared, it value is
 * 0(zero) by default.
 * 
 * 
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number: 111044016
 * 
 */
public class GITInterpreter {

	/**
	 * List of supported operators by GITLanguage
	 */
	private static final String OPERATORS = "+-*/=^";

	/**
	 * Regular expression for splitting lines from Whitespace characters
	 */
	private static final String SPLIT_REGEX = "\\s";

	private FileReader m_file;
	private ListIterator<String> m_lines;
	private Variables m_vars;
	private Stack<Variables> m_loopLocals;
	private int m_currLineNum;

	public GITInterpreter(String FileName) {
		m_file = new FileReader(FileName);
		m_lines = m_file.getListIterator();
		m_vars = new Variables();
		m_loopLocals = new Stack<>();
		m_file.numberOfLines();
		m_currLineNum = 0;
	}

	/**
	 * Evaluates currently open source file.
	 * 
	 * @throws SyntaxErrorException
	 *             If there is any Syntax Error happens
	 * @throws UnknownTypeException
	 *             If there is any type code missmatch happens
	 */
	public void eval() throws SyntaxErrorException, UnknownTypeException,
			MixedTypeException {

		String currLine = "";
		String tmpVarName = "";
		String lineBegin = "";

		while (m_lines.hasNext()) {

			currLine = m_lines.next();

			String toCheck = new String(currLine);

			if (isMixedType(toCheck)) {
				throw new MixedTypeException(m_currLineNum);
			}

			// Fix possible brackets related syntax errors
			currLine = currLine.replace("(", " ( ");
			currLine = currLine.replace(")", " ) ");

			// Fix possible operator related syntax errors
			currLine = currLine.replace("=", " = ");
			currLine = currLine.replace("+", " + ");
			currLine = currLine.replace("-", " - ");
			currLine = currLine.replace("*", " * ");
			currLine = currLine.replace("/", " / ");
			currLine = currLine.replace("^", " ^ ");

			currLine = currLine.trim();

			// If line is empty
			if (currLine.isEmpty() || currLine.equals("")
					|| currLine.equals("\n") || currLine.equals(" ")) {

				continue;
			}

			lineBegin = currLine.split(SPLIT_REGEX)[0];

			// check for variable definition
			if (lineBegin.equals(Keyword.INT)) {
				// int varaible definition
				// variable type is int
				tmpVarName = currLine.split(SPLIT_REGEX)[1];
				try {
					if (!m_vars.add(new Variable(Type.INT, tmpVarName,
							new Integer(0)))) {

						throw new SyntaxErrorException(m_currLineNum,
								"Redefinition of variable " + tmpVarName);
					}

				} catch (ClassCastException | DataTypeException e) {

					System.out.println(e.getMessage());
				}

				if ((currLine.split(SPLIT_REGEX).length) > 2) {
					throw new SyntaxErrorException(m_currLineNum,
							"Too Much Operands for varaible declaration");

				}

			} else if (lineBegin.equals(Keyword.FLOAT)) {
				// float variable def
				tmpVarName = currLine.split(SPLIT_REGEX)[1];

				try {
					if (!m_vars.add(new Variable(Type.FLOAT, tmpVarName,
							new Float(0.0)))) {

						throw new SyntaxErrorException(m_currLineNum,
								"Redefinition of variable " + tmpVarName);
					}
				} catch (ClassCastException | DataTypeException e) {

					System.out.println(e.getMessage());
				}

				if ((currLine.split(SPLIT_REGEX).length) > 2) {
					throw new SyntaxErrorException(m_currLineNum,
							"Too Much Operands for varaible declaration");
				}

			} else if (lineBegin.equals(Keyword.PRINT)) { // Print

				tmpVarName = currLine.split(SPLIT_REGEX)[1];

				if (!m_vars.hasVariable(tmpVarName)) {
					throw new SyntaxErrorException(m_currLineNum, "Variable "
							+ tmpVarName + " is not declared");
				}

				if ((currLine.split(SPLIT_REGEX).length) > 2) {
					throw new SyntaxErrorException(m_currLineNum,
							"Too Much Operands for \"print\"");
				}

				System.out.println(m_vars.get(tmpVarName).getValue());

			} else if (lineBegin.equals(Keyword.LOOP)) { // Loop

				String loopCond = currLine.split(SPLIT_REGEX)[1];

				String toParse = new String(loopCond);
				toParse = toParse.trim();

				String loopVarName = toParse.split(":")[0];
				String loopInc = toParse.split(":")[1];
				String loopEnd = toParse.split(":")[2];

				if (!isInt(loopInc)) {
					throw new SyntaxErrorException(m_currLineNum,
							"Loop invariant must be an int value.");
				}
				int loopIncVal = Integer.parseInt(loopInc);

				if (isInt(loopVarName)) {
					throw new SyntaxErrorException(m_currLineNum,
							"Loop Counter must be a variable");
				}

				Variable loopVar = m_vars.get(loopVarName);

				if (null == loopVar) {
					try {
						loopVar = new Variable(Type.INT, loopVarName,
								loopIncVal);

					} catch (ClassCastException | DataTypeException e) {
						throw new SyntaxErrorException(m_currLineNum,
								"Unknown Error. Error Massage by JVM:\n"
										+ e.getMessage());
					}
				}

				Variable loopEndVar;
				if (null != (loopEndVar = m_vars.get(loopEnd))) {
					if (Type.INT != loopEndVar.getType()) {
						throw new SyntaxErrorException(m_currLineNum,
								"Terminate condition of loop is not an"
										+ " int variable. ");
					}

				} else {
					if (!isInt(loopEnd)) {
						throw new SyntaxErrorException(m_currLineNum,
								"Terminate condition of loop is neither a "
										+ "variable nor an int value.");
					}
					try {
						loopEndVar = new Variable(Type.INT, "loopEnd",
								Integer.parseInt(loopEnd));

					} catch (NumberFormatException | ClassCastException
							| DataTypeException e) {

						e.printStackTrace();
					}
				}

				ArrayList<String> loopBody = new ArrayList<>();
				int loopCount = 0;

				String tmp = new String();
				while (m_lines.hasNext()) {
					tmp = m_lines.next();
					tmp = tmp.trim();

					if (tmp.equals(Keyword.BEGIN)) {
						++loopCount;
					} else if (tmp.equals(Keyword.END)) {
						--loopCount;
					} else if (0 == loopCount) {
						break;
					}
					loopBody.add(tmp);

				}
				if (0 != loopCount) {

					throw new SyntaxErrorException(m_currLineNum, "Missing \""
							+ Keyword.BEGIN + "\" or \"" + Keyword.END + "\""
							+ " at loop statement");
				}

				loopEval(loopBody, loopVar, loopEndVar, m_currLineNum);

				m_currLineNum += loopBody.size();

			} else if (m_vars.hasVariable(lineBegin)) {
				Variable toAssign = m_vars.get(lineBegin);

				// Get operator
				String op = "=";

				if (!currLine.contains(op)) {
					throw new SyntaxErrorException(m_currLineNum, "variable \""
							+ lineBegin + "\" used as Lvalue of "
							+ "a non-assignment operator");
				}

				StringTokenizer lineTest = new StringTokenizer(currLine);
				String currToken = "";
				int tokenCount = 0;

				while (lineTest.hasMoreTokens()) {
					++tokenCount;
					currToken = lineTest.nextToken();

					if ((2 == tokenCount) && (!currToken.equals("="))) {
						throw new SyntaxErrorException(m_currLineNum,
								"Missing operator \"=\"");
					}
				}

				// Get expression
				String exp = "";
				try {
					exp = currLine.substring(currLine.indexOf(op) + 1);

				} catch (StringIndexOutOfBoundsException e) {
					throw new SyntaxErrorException(m_currLineNum,
							"Invalid Syntax");
				}

				if (op.length() > 1) {
					throw new SyntaxErrorException(m_currLineNum,
							"Unknown operator " + op);
				}

				if (OPERATORS.contains(op)) {
					if (toAssign.getType() == Type.INT) {

						for (Variable var : m_vars) {
							if (Type.INT == var.getType()) {
								Integer i = (Integer) var.getValue();

								exp = exp.replace(var.getName(), i.toString());
							}

						}

						Integer result = (Integer) toAssign.getValue();

						//TODO error
						
						result = (Integer) parseExp(exp, Type.INT,
								m_currLineNum);

						toAssign.setValue(result);
					} else {

						for (Variable var : m_vars) {
							if (Type.FLOAT == var.getType()) {
								Float f = (Float) var.getValue();

								exp = exp.replace(var.getName(), f.toString());
							}

						}

						Float result = (Float) toAssign.getValue();

						result = (Float) parseExp(exp, Type.FLOAT,
								m_currLineNum);

						toAssign.setValue(result);
					}

				} else {

					throw new SyntaxErrorException(m_currLineNum,
							"Unknown operator " + op);
				}

			} else {

				if (lineBegin.equals("=")) {
					throw new SyntaxErrorException(m_currLineNum,
							"Assignment operator has no LValue");
				}
				throw new SyntaxErrorException(m_currLineNum, "\"" + lineBegin
						+ "\" is not defined");
			}

			++m_currLineNum;

		}

	}

	/**
	 * Takes loop part of source code then evaluates it.
	 * 
	 * @param bodyCode
	 *            an <tt>ArrayList</tt> object which contains lines of codes are
	 *            <tt>String</tt> objects.
	 * 
	 * @param loopVar
	 *            The Variable which is actually loop counter.
	 * @param TermCond
	 *            Terminal value of loop counter.
	 * @param lineBegin
	 *            Line number of beginning of loop at Source Code.
	 * 
	 * @return Last value of loop counter
	 * 
	 * @throws SyntaxErrorException
	 *             If there is any syntax error(s) occurs
	 */
	private int loopEval(ArrayList<String> bodyCode, Variable loopVar,
			Variable TermCond, int lineBegin) throws SyntaxErrorException {

		m_loopLocals.push(new Variables());
		// create local variables list and
		// add it to stack

		Variables localVars = m_loopLocals.pop();
		// take back local variables
		// list from stack

		int counter = 0;
		int currLine = 0;

		localVars.add(loopVar);

		for (Variable var : m_vars) {
			localVars.add(var);
		}

		if (!m_loopLocals.isEmpty()) {
			Iterator<Variables> tmpIter = m_loopLocals.iterator();
			while (tmpIter.hasNext()) {
				for (Variable var : tmpIter.next()) {
					localVars.add(var);
				}
			}

		}

		ListIterator<String> loopLines = bodyCode.listIterator();

		while ((Integer) loopVar.getValue() <= (Integer) TermCond.getValue()) {
			// TODO loop operations
			// TODO add nested loop structure
			currLine = lineBegin;

			boolean loopVarUpdate = false;

			while (loopLines.hasNext()) {
				++currLine;

				String line = loopLines.next();
				line = line.trim();

				String loopLineBegin = line.split(SPLIT_REGEX)[0];
				String tmpVarName = "";

				if (loopLineBegin.equals(Keyword.PRINT)) {
					// Print

					tmpVarName = line.split(SPLIT_REGEX)[1];

					if (!localVars.hasVariable(tmpVarName)) {
						throw new SyntaxErrorException(currLine, "Variable "
								+ tmpVarName + " is not declared");
					}

					if ((line.split(SPLIT_REGEX).length) > 2) {
						throw new SyntaxErrorException(currLine,
								"Too Much Operands for \"print\"");
					}

					System.out.println(localVars.get(tmpVarName).getValue());
				} else if (loopLineBegin.equals(Keyword.INT)) {

					if (counter == 0) {

						tmpVarName = line.split(SPLIT_REGEX)[1];
						Variable tmpVar = null;
						try {
							tmpVar = new Variable(Type.INT, tmpVarName,
									new Integer(0));

						} catch (ClassCastException | DataTypeException e1) {
							e1.printStackTrace();
						}
						if (m_vars.hasVariable(tmpVarName)) {

							Variable tmp = m_vars.get(tmpVarName);

							if (tmp.getType() == Type.INT) {
								tmpVar = tmp;
							}
						}

						if (!localVars.add(tmpVar)) {
							throw new SyntaxErrorException(currLine,
									"Redefinition of variable " + tmpVarName);
						}

						if ((line.split(SPLIT_REGEX).length) > 2) {

							throw new SyntaxErrorException(currLine,
									"Too Much Operands for"
											+ " varaible declaration");
						}

					}

				} else if (loopLineBegin.equals(Keyword.FLOAT)) {
					// float
					// variable
					if (counter == 0) {

						tmpVarName = line.split(SPLIT_REGEX)[1];

						Variable tmpVar = null;
						try {
							tmpVar = new Variable(Type.FLOAT, tmpVarName,

							new Integer(0));
						} catch (ClassCastException | DataTypeException e1) {
							e1.printStackTrace();
						}
						if (m_vars.hasVariable(tmpVarName)) {

							Variable tmp = m_vars.get(tmpVarName);

							if (tmp.getType() == Type.FLOAT) {
								tmpVar = tmp;
							}
						}

						if (!localVars.add(tmpVar)) {
							throw new SyntaxErrorException(currLine,
									"Redefinition of variable " + tmpVarName);
						}

						if ((line.split(SPLIT_REGEX).length) > 2) {

							throw new SyntaxErrorException(currLine,
									"Too Much Operands for"
											+ " varaible declaration");
						}
					}
				} else if (localVars.hasVariable(loopLineBegin)) {

					Variable toAssign = localVars.get(loopLineBegin);

					if (loopVar == toAssign) {
						loopVarUpdate = true;
					} else {
						loopVarUpdate = false;
					}

					// Get operator
					String op = "=";

					if (!line.contains(op)) {
						throw new SyntaxErrorException(currLine, "variable \""
								+ lineBegin + "\" used as Lvalue of "
								+ "a non-assignment operator");
					}

					StringTokenizer lineTest = new StringTokenizer(line);
					String currToken = "";
					int tokenCount = 0;

					while (lineTest.hasMoreTokens()) {
						++tokenCount;
						currToken = lineTest.nextToken();

						if ((2 == tokenCount) && (!currToken.equals("="))) {

							throw new SyntaxErrorException(currLine,
									"Missing operator \"=\"");
						}
					}

					// Get expression
					String exp = "";
					try {
						exp = line.substring(line.indexOf(op) + 1);

					} catch (StringIndexOutOfBoundsException e) {

						throw new SyntaxErrorException(currLine,
								"Invalid Syntax");
					}

					if (op.length() > 1) {
						throw new SyntaxErrorException(currLine,
								"Unknown operator " + op);
					}

					if (OPERATORS.contains(op)) {
						if (toAssign.getType() == Type.INT) {

							for (Variable var : localVars) {

								if (Type.INT == var.getType()) {

									Integer i = (Integer) var.getValue();

									exp = exp.replace(var.getName(),
											i.toString());
								}

							}

							Integer result = (Integer) toAssign.getValue();

							try {
								result = (Integer) parseExp(exp, Type.INT,
										m_currLineNum);

							} catch (UnknownTypeException e) {

								e.printStackTrace();
							}

							toAssign.setValue(result);
						} else {

							for (Variable var : localVars) {
								if (Type.FLOAT == var.getType()) {

									Float f = (Float) var.getValue();

									exp = exp.replace(var.getName(),
											f.toString());
								}

							}

							Float result = (Float) toAssign.getValue();

							try {
								result = (Float) parseExp(exp, Type.FLOAT,
										currLine);

							} catch (UnknownTypeException e) {
								e.printStackTrace();
							}

							toAssign.setValue(result);
						}

					} else {

						throw new SyntaxErrorException(currLine,
								"Unknown operator " + op);
					}

				} else if (loopLineBegin.equals(Keyword.BEGIN)) {

					continue;

				} else if (loopLineBegin.equals(Keyword.END)) {
					continue;
				} else if (loopLineBegin.equals(Keyword.LOOP)) {
					// Loop

					String loopCond = line.split(SPLIT_REGEX)[1];

					String toParse = new String(loopCond);
					toParse = toParse.trim();

					String loopVarName = toParse.split(":")[0];

					String loopInc = toParse.split(":")[1];
					String loopEnd = toParse.split(":")[2];

					if (!isInt(loopInc)) {
						throw new SyntaxErrorException(currLine,
								"Loop invariant must be an int value.");
					}
					int loopIncVal = Integer.parseInt(loopInc);

					if (isInt(loopVarName)) {
						throw new SyntaxErrorException(currLine,
								"Loop Counter must be a variable");
					}

					Variable nestLoopVar = localVars.get(loopVarName);

					if (null == nestLoopVar) {
						try {
							nestLoopVar = new Variable(Type.INT, loopVarName,
									loopIncVal);

						} catch (ClassCastException | DataTypeException e) {

							throw new SyntaxErrorException(currLine,
									"Unknown Error. Error Massage by JVM:\n"
											+ e.getMessage());
						}
					} else if (nestLoopVar.getType() == Type.INT) {
						nestLoopVar.setValue(loopIncVal);
					} else {
						throw new SyntaxErrorException(currLine,
								"Loop counter type must be \"int\"");
					}

					Variable loopEndVar;
					if (null != (loopEndVar = m_vars.get(loopEnd))) {

						if (Type.INT != loopEndVar.getType()) {
							throw new SyntaxErrorException(m_currLineNum,
									"Terminate condition of loop is not an"
											+ " int variable. ");
						}

					} else {
						if (!isInt(loopEnd)) {
							throw new SyntaxErrorException(m_currLineNum,
									"Terminate condition of loop is neither a "
											+ "variable nor an int value.");
						}

						try {
							loopEndVar = new Variable(Type.INT, "loopEnd",
									Integer.parseInt(loopEnd));

						} catch (NumberFormatException | ClassCastException
								| DataTypeException e) {
							e.printStackTrace();
						}
					}

					ArrayList<String> loopBody = new ArrayList<>();

					int loopCount = 0;

					String tmp = new String();
					while (loopLines.hasNext()) {
						tmp = loopLines.next();
						tmp = tmp.trim();

						if (tmp.equals(Keyword.BEGIN)) {
							++loopCount;
							if (0 == loopCount) {
								throw new SyntaxErrorException(currLine,
										"\"Begin\" after \"End\"");
							}
						} else if (tmp.equals(Keyword.END)) {
							if (0 == loopCount) {
								break;
							}
							--loopCount;
						} else if (0 == loopCount) {
							break;
						}
						loopBody.add(tmp);

					}

					if (0 != loopCount) {

						throw new SyntaxErrorException(currLine, "Missing \""
								+ Keyword.BEGIN + "\" or \"" + Keyword.END
								+ "\"" + " at loop statement");
					}
					m_loopLocals.push(localVars);
					loopEval(loopBody, nestLoopVar, loopEndVar, currLine);
					m_loopLocals.pop();

				} else {

					if (line.equals("=")) {
						throw new SyntaxErrorException(currLine,
								"Assignment operator has no LValue");
					}
					throw new SyntaxErrorException(currLine, "\"" + line
							+ "\" is not defined");
				}

			}

			rewindListIter(loopLines);

			if (!loopVarUpdate) {
				loopVar.setValue((Integer) loopVar.getValue() + 1);
			}
		}

		return ((Integer) loopVar.getValue());
	}

	/**
	 * Parses given expression then calls related evaluator class to evaluate
	 * it.
	 * 
	 * @param line
	 *            Expression to evaluate
	 * 
	 * @return Result of Expression
	 * 
	 * @throws SyntaxErrorException
	 *             If there is any Syntax Error happens
	 * @throws UnknownTypeException
	 *             If there is any type code mismatch happens
	 */
	private Number parseExp(String line, int type, int lineNo)
			throws SyntaxErrorException, UnknownTypeException {

		if (Type.INT == type) {

			InfixToPostfix coverter = new InfixToPostfix();
			IntegerPostfixEvaluator evaluator = new IntegerPostfixEvaluator();
			Integer result = null;
			try {
				String postfix = coverter.convert(line);
				result = evaluator.eval(postfix);

			} catch (ConvertErrorException e) {
				throw new SyntaxErrorException(lineNo, e.getMessage());
			} catch (EvalErrorException e) {

				String msg = "";

				if (e.getMessage().equals(
						IntegerPostfixEvaluator.STACK_EMPTY_ERR)) {

					msg = "Missing operand at expression";
				} else if (e.getMessage().equals(
						IntegerPostfixEvaluator.STACK_NOT_EMPTY_ERR)) {

					msg = "Too many operands for expression";
				} else {
					msg = e.getMessage();

				}

				throw new SyntaxErrorException(lineNo, msg);
			}

			return result;

		} else if (Type.FLOAT == type) {
			InfixToPostfix converter = new InfixToPostfix();
			FloatPostfixEvaluator evaluator = new FloatPostfixEvaluator();
			Float result = null;

			try {
				String postfix = converter.convert(line);
				result = evaluator.eval(postfix);

			} catch (ConvertErrorException e) {
				throw new SyntaxErrorException(lineNo, e.getMessage());
			} catch (FloatEvalErrorException e) {

				String msg = "";

				if (e.getMessage()
						.equals(FloatPostfixEvaluator.STACK_EMPTY_ERR)) {

					msg = "Missing operand at expression";
				} else if (e.getMessage().equals(
						FloatPostfixEvaluator.STACK_NOT_EMPTY_ERR)) {

					msg = "Too many operands for expression";
				} else {
					msg = e.getMessage();
				}

				throw new SyntaxErrorException(lineNo, msg);
			}

			return result;
		}

		throw new UnknownTypeException(type);

	}

	/**
	 * Checks given expression. If expression is a mixed type expresion
	 * (contains both float and int variables) returns <tt>true</tt>, else
	 * returns <tt>false</tt>
	 * 
	 * @param exp
	 *            Expression to check
	 * @return If expression is a mixed type expression returns <tt>true</tt>,
	 *         else returns <tt>false</tt>
	 */
	private boolean isMixedType(String exp) {

		String orig = new String(exp);
		boolean hasInt = false;
		boolean hasFloat = false;

		for (Variable var : m_vars) {
			if (var.getType() == Type.INT) {
				exp = exp.replace(var.getName(), var.getValue().toString());
			}
		}

		if (!exp.equals(orig)) {
			hasInt = true;
		}

		orig = new String(exp);

		for (Variable var : m_vars) {
			if (var.getType() == Type.FLOAT) {
				exp = exp.replace(var.getName(), var.getValue().toString());
			}
		}

		if (!exp.equals(orig)) {
			hasFloat = true;
		}

		return (hasFloat && hasInt);
	}

	/**
	 * Checks given string for is it completely int.
	 * 
	 * @return <tt>true</tt> if given string is a int number, else
	 *         <tt<false</tt>.
	 */
	private boolean isInt(String toCheck) {
		try {
			Integer.parseInt(toCheck);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	/**
	 * Rewinds a {@link String} list's {@link ListIterator} to its beginning
	 * 
	 * @param listIter
	 *            {@link ListIterator} object for a {@link String} list to
	 *            rewind.
	 */
	private void rewindListIter(ListIterator<String> listIter) {
		while (listIter.hasPrevious()) {
			listIter.previous();
		}
	}

	/**
	 * CSE222 - HW04 - GITLanguage - Syntax Error Exception Class of
	 * GITInterpreter. This exception will be thrown when there is a Syntax
	 * Error in source code.
	 * 
	 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number:
	 *         111044016
	 */
	public static final class SyntaxErrorException extends Exception {

		private static final long serialVersionUID = 847185371220819767L;

		public SyntaxErrorException(int lineNumber, String message) {
			super("Syntax Error in line #" + (lineNumber + 1) + ": " + message);
		}
	}

	/**
	 * CSE222 - HW04 - GITLanguage - Mixed Type Exception Class of
	 * GITInterpreter. This exception will be thrown when there is an operation
	 * with mixed type.
	 * 
	 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> Student Number:
	 *         111044016
	 */
	public static final class MixedTypeException extends Exception {

		private static final long serialVersionUID = 56286718524605835L;

		public MixedTypeException(int lineNumber) {
			super("Mixed type operation in line #" + (lineNumber + 1));
		}
	}

}
